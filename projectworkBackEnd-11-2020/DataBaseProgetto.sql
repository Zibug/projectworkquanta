-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: projectwork
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adm`
--

DROP TABLE IF EXISTS `adm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adm` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Acc` varchar(45) NOT NULL,
  `Psw` varchar(45) NOT NULL,
  `Ad` tinyint(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm`
--

LOCK TABLES `adm` WRITE;
/*!40000 ALTER TABLE `adm` DISABLE KEYS */;
INSERT INTO `adm` VALUES (1,'admin1','adminpsw1',1),(2,'admin2','adminpsw2',1),(20,'zibug94','password',0);
/*!40000 ALTER TABLE `adm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat`
--

DROP TABLE IF EXISTS `cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cat` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat`
--

LOCK TABLES `cat` WRITE;
/*!40000 ALTER TABLE `cat` DISABLE KEYS */;
INSERT INTO `cat` VALUES (3,'videogiochi'),(5,'Libri'),(6,'elettronica');
/*!40000 ALTER TABLE `cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imm`
--

DROP TABLE IF EXISTS `imm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `imm` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Url` varchar(1000) NOT NULL,
  `Pri` tinyint(1) NOT NULL,
  `prod_id` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `prod_id` (`prod_id`),
  CONSTRAINT `imm_ibfk_1` FOREIGN KEY (`prod_id`) REFERENCES `prod` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imm`
--

LOCK TABLES `imm` WRITE;
/*!40000 ALTER TABLE `imm` DISABLE KEYS */;
INSERT INTO `imm` VALUES (30,'https://dday.imgix.net/system/uploads/news/main_image/36903/af1114_ps5dig.jpg?ar=702%3A360&fit=crop&auto=format%2Ccompress&w=702&s=ac1f12a6f3d7394a5b56243c194ad4a2',0,2),(31,'https://www.techinn.com/f/13777/137776929/microsoft-xbox-series-x-1tb.jpg',1,12),(34,'https://www.opplastore.it/wp-content/uploads/2019/04/gioco-per-il-cavallo-1024x626.jpg',1,15),(45,'https://www.oscarmondadori.it/content/uploads/2019/04/978880471195HIG-333x480.jpg',1,22),(46,'https://3.bp.blogspot.com/-eoJppajzouc/WoMIFwvLiUI/AAAAAAABpZs/AT7qYmvTleIzIcCgcjpbpB_tRxtXC7UKQCLcBGAs/s400/computer-scegliere.jpg',1,23),(47,'https://images.everyeye.it/img-notizie/ps5-arriva-aggiornamento-20-02-02-50-00-risolve-bug-dell-installazione-giochi-ps4-v3-496997-1280x720.jpg',1,2);
/*!40000 ALTER TABLE `imm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ind`
--

DROP TABLE IF EXISTS `ind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ind` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `via` varchar(45) NOT NULL,
  `num` int NOT NULL,
  `cap` int NOT NULL,
  `pae` varchar(45) NOT NULL,
  `userCred_id` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `userCred_id` (`userCred_id`),
  CONSTRAINT `ind_ibfk_1` FOREIGN KEY (`userCred_id`) REFERENCES `usercred` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ind`
--

LOCK TABLES `ind` WRITE;
/*!40000 ALTER TABLE `ind` DISABLE KEYS */;
INSERT INTO `ind` VALUES (15,'Giancarlo Sismondi ',43,20133,'Italia',15);
/*!40000 ALTER TABLE `ind` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod`
--

DROP TABLE IF EXISTS `prod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prod` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL,
  `Quant` int NOT NULL,
  `Prezzo` int NOT NULL,
  `Des` varchar(45) NOT NULL,
  `cat_id` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `cat_id` (`cat_id`),
  CONSTRAINT `prod_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `cat` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod`
--

LOCK TABLES `prod` WRITE;
/*!40000 ALTER TABLE `prod` DISABLE KEYS */;
INSERT INTO `prod` VALUES (2,'playstation',15,500,'descPlay',3),(12,'xbox',30,1,'paura',3),(15,'ciaociao',345,41241243,'cavallo',3),(22,'game of thrones',35,5,'Libro Fantasy',5),(23,'computer',100,1200,'veloce',6);
/*!40000 ALTER TABLE `prod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodcart`
--

DROP TABLE IF EXISTS `prodcart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prodcart` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quant` int NOT NULL,
  `prod_id` int NOT NULL,
  `uca_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uca_id` (`uca_id`),
  CONSTRAINT `prodcart_ibfk_1` FOREIGN KEY (`uca_id`) REFERENCES `usercart` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodcart`
--

LOCK TABLES `prodcart` WRITE;
/*!40000 ALTER TABLE `prodcart` DISABLE KEYS */;
/*!40000 ALTER TABLE `prodcart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usercart`
--

DROP TABLE IF EXISTS `usercart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usercart` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `usercart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `adm` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usercart`
--

LOCK TABLES `usercart` WRITE;
/*!40000 ALTER TABLE `usercart` DISABLE KEYS */;
/*!40000 ALTER TABLE `usercart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usercred`
--

DROP TABLE IF EXISTS `usercred`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usercred` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL,
  `Sur` varchar(45) NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `usercred_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `adm` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usercred`
--

LOCK TABLES `usercred` WRITE;
/*!40000 ALTER TABLE `usercred` DISABLE KEYS */;
INSERT INTO `usercred` VALUES (15,'Christian Kevin','Sibug',20);
/*!40000 ALTER TABLE `usercred` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-21  9:20:48
