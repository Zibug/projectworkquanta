package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Cat;
import com.quanta.model.Imm;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOCatImpl;

/**
 * Servlet implementation class MainServlet2
 */
public class MainServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOCat dAOCat = new DAOCatImpl();
	List<Cat> listCat = new ArrayList<Cat>();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		listCat = dAOCat.findAll();
		boolean login = (boolean)request.getSession().getAttribute("login");
		request.setAttribute("login", login);
		request.setAttribute("listCat", listCat);
		String userName = null;
		if(request.getSession().getAttribute("userName") != null) {
			userName = (String)request.getSession().getAttribute("userName");
		}
		request.setAttribute("userName", userName);
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	}

}
