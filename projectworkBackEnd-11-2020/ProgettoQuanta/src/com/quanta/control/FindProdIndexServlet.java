package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.persistence.impl.DAOCatImpl;
import com.quanta.persistence.impl.DAOProdImpl;
import com.quanta.model.Prod;
import com.quanta.model.ProdCart;
import com.quanta.model.UserCart;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.DAOProd;

/**
 * Servlet implementation class FindProdIndexServlet
 */
public class FindProdIndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	DAOCat dAOCat = new DAOCatImpl();
	List<Prod> listProd = new ArrayList<Prod>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cId = Integer.parseInt(request.getParameter("cId"));
		boolean login = (boolean)request.getSession().getAttribute("login");
		String cNome = dAOCat.getCName(cId);
		listProd = dAOProd.FindAll(cId);
		request.setAttribute("login", login);
		request.setAttribute("cNome", cNome);
		request.setAttribute("cId", cId);
		request.setAttribute("listProd", listProd);
		RequestDispatcher rd = request.getRequestDispatcher("prodottiIndex.jsp");
		rd.forward(request, response);
	}
	
}
