package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOCatImpl;


/**
 * Servlet implementation class EditCatServlet
 */
@WebServlet("/EditCatServlet")
public class EditCatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOCat dAOCat = new DAOCatImpl();
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cId = Integer.parseInt(request.getParameter("id"));
		String cNome = dAOCat.getCName(cId);
		boolean errore = false;
		request.setAttribute("errore", errore);
		request.setAttribute("cId", cId);
		request.setAttribute("cNome", cNome);
		RequestDispatcher rd = request.getRequestDispatcher("editCat.jsp");
		rd.forward(request, response);
	}

}
