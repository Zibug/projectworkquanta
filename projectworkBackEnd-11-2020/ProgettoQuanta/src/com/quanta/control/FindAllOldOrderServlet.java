package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.model.ProdCart;
import com.quanta.model.UserCart;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.DAOUserCart;
import com.quanta.persistence.impl.DAOProdImpl;
import com.quanta.persistence.impl.DAOUserCartImpl;

/**
 * Servlet implementation class FindAllOldOrderServlet
 */
@WebServlet("/FindAllOldOrderServlet")
public class FindAllOldOrderServlet extends HttpServlet {
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DAOUserCart dAOUserCart = new DAOUserCartImpl();
		DAOProd dAOProd = new DAOProdImpl();
		String admState = request.getParameter("admState");
		List<UserCart> listaOrdini = new ArrayList<UserCart>(); 
		//devo trasformare la lista ordini in tipo Prod per poter stampare tutte le info che mi servono nel jsp
		int uId = 0;
		if(admState.equals("true")) {
			uId = Integer.parseInt(request.getParameter("idUser"));
		} else {
			uId = (Integer)request.getSession().getAttribute("idUser");
		}
		listaOrdini = dAOUserCart.FindAllByuId(uId);
		for(UserCart userCart:listaOrdini) {
			if(userCart != null) {
				for(ProdCart datiProdotto:userCart.getListProdCart()) {
					Prod prodotto = dAOProd.FindProd(datiProdotto.getpId());
					prodotto.setpQuant(datiProdotto.getPcQuant());
					userCart.getListProdPrint().add(prodotto);
				}
			}
		}
		if(!request.getParameter("aName").equals("false")) {
			String aName = request.getParameter("aName");
			request.setAttribute("aName", aName);
		}
		request.setAttribute("admState", admState);
		request.setAttribute("listaOrdini", listaOrdini);
		RequestDispatcher rd = request.getRequestDispatcher("ordiniUtente.jsp");
		rd.forward(request, response);
	}

}
