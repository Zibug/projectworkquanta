package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.persistence.DAOCat;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOCatImpl;
import com.quanta.persistence.impl.DAOProdImpl;
import com.quanta.model.*;

/**
 * Servlet implementation class DeleteCatServlet
 */
@WebServlet("/DeleteCatServlet")
public class DeleteCatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOCat dAOCat = new DAOCatImpl();
	DAOProd dAOProd = new DAOProdImpl();
	List<Prod> listProd = new ArrayList<Prod>();
	List<Cat> listCat = new ArrayList<Cat>();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cId = Integer.parseInt(request.getParameter("cId"));
		listProd = dAOProd.FindAll(cId);
		for(int i = 0; i < listProd.size(); i++) {
			int pId = listProd.get(i).getpId();
			dAOProd.deleteAllImm(pId);
		}
		dAOCat.DeleteCat(cId);
		listCat = dAOCat.findAll();
		request.setAttribute("lista", listCat);
		RequestDispatcher rd = request.getRequestDispatcher("categorie.jsp");
		rd.forward(request, response);
	}

}



















