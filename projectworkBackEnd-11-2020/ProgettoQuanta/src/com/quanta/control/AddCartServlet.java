package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.model.ProdCart;
import com.quanta.model.UserCart;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.DAOProdCart;
import com.quanta.persistence.impl.DAOProdCartImpl;
import com.quanta.persistence.impl.DAOProdImpl;

/**
 * Servlet implementation class AddCartServlet
 */
@WebServlet("/AddCartServlet")
public class AddCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	DAOProdCart dAOProdCart = new DAOProdCartImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserCart userCart = (UserCart)request.getSession().getAttribute("carrello");
		//Login
		boolean login = (boolean)request.getSession().getAttribute("login");
		request.setAttribute("login", login);
		if(userCart == null) {
			userCart = new UserCart();
			request.getSession().setAttribute("carrello", userCart);
		}
		List<ProdCart> listProdCart = userCart.getListProdCart();
		//pId = id prodotto da aggiungere
		int pId = Integer.parseInt(request.getParameter("pId"));
		// pcQuant = quantita prodotto da aggiungere
		int pcQuant = Integer.parseInt(request.getParameter("pcQuant"));
		// url = indirzzo immagine con id del prodotto = pId
		String url = dAOProd.FindImmUrl(pId);
		// prod = prodotto con id = pId
		Prod prod = dAOProd.FindProd(pId);
		//ricerca esistenza prodotto nel carrello
		ProdCart prodCartF = null;
		//listProdCart = dAOProdCart.FindAll();
		for(int i = 0 ; i < listProdCart.size(); i++) {
			if(listProdCart.get(i).getpId() == pId) {
				prodCartF = listProdCart.get(i);
				break;
			}
		}
		//if login
		if(prodCartF != null) {
			prodCartF.setPcQuant(prodCartF.getPcQuant() + pcQuant);
			request.setAttribute("url", url);
			String pcNome = prod.getpName();
			request.setAttribute("pcNome", pcNome);
			RequestDispatcher rd = request.getRequestDispatcher("prodAdded.jsp");
			rd.forward(request, response);
		} else {
			prodCartF = new ProdCart();
			request.setAttribute("url", url);
			String pcNome = prod.getpName();
			request.setAttribute("pcNome", pcNome);
			prodCartF.setPcQuant(pcQuant);
			prodCartF.setpId(pId);
			//prodCartF.setPcNome(pcNome);
			listProdCart.add(prodCartF);
			RequestDispatcher rd = request.getRequestDispatcher("prodAdded.jsp");
			rd.forward(request, response);
		}
	}
}









