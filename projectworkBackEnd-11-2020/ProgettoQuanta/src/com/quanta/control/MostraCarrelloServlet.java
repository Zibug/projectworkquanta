package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.UserCart;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOProdImpl;
import com.quanta.model.Prod;
import com.quanta.model.ProdCart;

import java.util.List;
import java.util.ArrayList;

/**
 * Servlet implementation class MostraCarrelloServlet
 */
@WebServlet("/MostraCarrelloServlet")
public class MostraCarrelloServlet extends HttpServlet {
	DAOProd dAOProd = new DAOProdImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserCart userCart = (UserCart)request.getSession().getAttribute("carrello");
		if(userCart != null) {
			List<Prod> listaProdottiNelCarrello = new ArrayList<Prod>();
			for(ProdCart datiProdotto:userCart.getListProdCart()) {
				Prod prodotto = dAOProd.FindProd(datiProdotto.getpId());
				prodotto.setpQuant(datiProdotto.getPcQuant());
				listaProdottiNelCarrello.add(prodotto);
			}
			request.setAttribute("listaProdottiNelCarrello", listaProdottiNelCarrello);
		}
		boolean login = (boolean)request.getSession().getAttribute("login");
		request.setAttribute("login", login);
		RequestDispatcher rd = request.getRequestDispatcher("carrello.jsp"); 
		rd.forward(request, response);
		System.out.println(userCart);
	}

}
