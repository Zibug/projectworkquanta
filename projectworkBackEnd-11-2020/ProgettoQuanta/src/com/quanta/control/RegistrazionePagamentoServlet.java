package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Adm;
import com.quanta.model.Indirizzo;
import com.quanta.model.UserCred;
import com.quanta.persistence.DAOAdmin;
import com.quanta.persistence.impl.DAOAdminImpl;

/**
 * Servlet implementation class RegistrazionePagamentoServlet
 */
@WebServlet("/RegistrazionePagamentoServlet")
public class RegistrazionePagamentoServlet extends HttpServlet {
	DAOAdmin dAOAdmin = new DAOAdminImpl();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserCred userCred = new UserCred();
		if(request.getParameter("user").isEmpty() || request.getParameter("pass").isEmpty() || request.getParameter("pass2").isEmpty() || request.getParameter("nome").isEmpty() || request.getParameter("cognome").isEmpty() || request.getParameter("via").isEmpty() || request.getParameter("numeroVia") == null || request.getParameter("cap") == null || request.getParameter("paese").isEmpty()) {
			String passwordUguali = "true";
			String erroreDatiRegistrazione = "true";
			String erroreDatiInseriti = "false";
			request.setAttribute("passwordUguali", passwordUguali);
			request.setAttribute("erroreDatiRegistrazione", erroreDatiRegistrazione);
			request.setAttribute("erroreDatiInseriti", erroreDatiInseriti);
			RequestDispatcher rd = request.getRequestDispatcher("loginPagamento.jsp");
			rd.forward(request, response);
		} else {
			String user = request.getParameter("user");
			String pass = request.getParameter("pass");
			String pass2 = request.getParameter("pass2");
			if(!pass.equals(pass2)) {
				String passwordUguali = "false";
				String erroreDatiRegistrazione = "false";
				String erroreDatiInseriti = "false";
				request.setAttribute("passwordUguali", passwordUguali);
				request.setAttribute("erroreDatiRegistrazione", erroreDatiRegistrazione);
				request.setAttribute("erroreDatiInseriti", erroreDatiInseriti);
				RequestDispatcher rd = request.getRequestDispatcher("loginPagamento.jsp");
				rd.forward(request, response);
			} else {
				String nome = request.getParameter("nome");
				String cognome = request.getParameter("cognome");
				String via = request.getParameter("via");
				int numeroVia = Integer.parseInt(request.getParameter("numeroVia"));
				int cap = Integer.parseInt(request.getParameter("cap"));
				String paese = request.getParameter("paese");
				userCred.setaName(user);
				userCred.setaPsw(pass);
				userCred.setNome(nome);
				userCred.setCognome(cognome);
				Indirizzo indirizzo = new Indirizzo(via, numeroVia, cap, paese);
				userCred.setIndirizzo(indirizzo);
				dAOAdmin.InsertUser(userCred);
				List<Adm> uList = new ArrayList<Adm>();
				uList = dAOAdmin.FindUser(user);
				Adm userF = null;
				for(Adm usr : uList) {
					if(pass.equals(usr.getaPsw())) {
						userF = usr;
					}
				}
				request.getSession().setAttribute("idUser", userF.getaId());
				boolean login = true;
				request.getSession().setAttribute("userName", user);
				request.getSession().setAttribute("login", login);
				String registrazioneAvvenuta = "true";
				request.setAttribute("userName", user);
				request.setAttribute("registrazioneAvvenuta", registrazioneAvvenuta);
				RequestDispatcher rd = request.getRequestDispatcher("pagamento.jsp");
				rd.forward(request, response);
			}
		}
	}

}
