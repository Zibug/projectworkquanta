package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Cat;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOCatImpl;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
	
	DAOCat dAOCat = new DAOCatImpl();
	List<Cat> listCat = new ArrayList<Cat>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean login = false;
		request.getSession().setAttribute("login", login);
		String userName = null;
		request.getSession().setAttribute("userName", userName);
		int idUser = 0;
		request.getSession().setAttribute("idUser", idUser);
		//request.getSession().invalidate()
		/*
		request.setAttribute("login", login);
		request.setAttribute("userName", userName);
		listCat = dAOCat.findAll();
		request.setAttribute("listCat", listCat);
		
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
		*/
		response.sendRedirect("main2");
	}
}
