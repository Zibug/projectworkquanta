package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Cat;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOCatImpl;

/**
 * Servlet implementation class AddCatServlet2
 */
@WebServlet("/AddCatServlet2")
public class AddCatServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
    DAOCat dAOCat = new DAOCatImpl();
    List<Cat> listCat = new ArrayList<Cat>();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("cNome").isEmpty()) {
			boolean errore = true;
			request.setAttribute("errore", errore);
			RequestDispatcher rd = request.getRequestDispatcher("addCat.jsp");
			rd.forward(request, response);
		} else {
			String cNome = request.getParameter("cNome");
			dAOCat.AddCat(cNome);
			listCat = dAOCat.findAll();
			request.setAttribute("lista", listCat);
			RequestDispatcher rd = request.getRequestDispatcher("categorie.jsp");
			rd.forward(request, response);
		}
	}
}
