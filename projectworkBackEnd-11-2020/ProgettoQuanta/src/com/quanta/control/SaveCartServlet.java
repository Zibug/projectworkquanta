package com.quanta.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.UserCart;
import com.quanta.persistence.DAOUserCart;
import com.quanta.persistence.impl.DAOUserCartImpl;

/**
 * Servlet implementation class SaveCartServlet
 */
@WebServlet("/SaveCartServlet")
public class SaveCartServlet extends HttpServlet {
	DAOUserCart dAOUserCart = new DAOUserCartImpl();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserCart userCart = (UserCart)request.getSession().getAttribute("carrello");
		int uId = (Integer)request.getSession().getAttribute("idUser");
		System.out.println(uId);
		dAOUserCart.AddUserCart(userCart, uId);
		userCart = null;
		request.getSession().setAttribute("carrello", userCart);
		response.sendRedirect("main2");
	}

}
