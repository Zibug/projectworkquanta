package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOProdImpl;
import com.quanta.model.*;

/**
 * Servlet implementation class FindProdServlet
 */
@WebServlet("/FindProdServlet")
public class FindProdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	List<Prod> listProd = new ArrayList<Prod>();
	//Domanda: Spostando listProdFind sistema pagina ricerca
	//List<Prod> listProdFind = new ArrayList<Prod>();
	
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//se tolgo qua listProdFind macello
		List<Prod> listProdFind = new ArrayList<Prod>();
		String ricerca = request.getParameter("ricerca").toLowerCase();
		boolean login = (boolean)request.getSession().getAttribute("login");
		listProd = dAOProd.FindAll();
		boolean trovato = false;
		for(Prod prod : listProd) {
			if(ricerca.equals(prod.getpName())) {
				trovato = true;
				listProdFind.add(prod);
			}
		}
		if(trovato) {
			request.setAttribute("login", login);
			request.setAttribute("trovato", trovato);
			request.setAttribute("listProdFind", listProdFind);
			RequestDispatcher rd = request.getRequestDispatcher("ricercaProd.jsp");
			rd.forward(request, response);
		} else {
			request.setAttribute("login", login);
			request.setAttribute("trovato", trovato);
			RequestDispatcher rd = request.getRequestDispatcher("ricercaProd.jsp");
			rd.forward(request, response);
		}
		
	}


}
