package com.quanta.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOCatImpl;
import com.quanta.persistence.impl.DAOProdImpl;

/**
 * Servlet implementation class AddProdServlet2
 */
@WebServlet("/AddProdServlet2")
public class AddProdServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	DAOCat dAOCat = new DAOCatImpl();
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cId = Integer.parseInt(request.getParameter("cId"));
		String pNome = request.getParameter("pName").toLowerCase();
		String pDesc = request.getParameter("pDesc");
		String url = request.getParameter("url");
		String cName = dAOCat.getCName(cId);
		List<Prod> listProd = dAOProd.FindAll(cId);
		if(pNome.isEmpty() || pDesc.isEmpty() || request.getParameter("pQuant").isEmpty() || request.getParameter("pPrezzo").isEmpty() || url.isEmpty()) {
			boolean errore = true;
			request.setAttribute("errore", errore);
			request.setAttribute("cId", cId);
			request.setAttribute("cName", cName);
			request.setAttribute("listProd", listProd);
			RequestDispatcher rd = request.getRequestDispatcher("addProd.jsp");
			rd.forward(request, response);
		} else {
			int pQuant = Integer.parseInt(request.getParameter("pQuant"));
			int pPrezzo = Integer.parseInt(request.getParameter("pPrezzo"));
			Prod prod = new Prod(pNome, pDesc, pQuant, pPrezzo);
			dAOProd.addProd(prod, cId, url);
			request.setAttribute("cId", cId);
			request.setAttribute("cName", cName);
			listProd = dAOProd.FindAll(cId);
			request.setAttribute("listProd", listProd);
			RequestDispatcher rd = request.getRequestDispatcher("prodotti.jsp");
			rd.forward(request, response);
		}
	}

}
