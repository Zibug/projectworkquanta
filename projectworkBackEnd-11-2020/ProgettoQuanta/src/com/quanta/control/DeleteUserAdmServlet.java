package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Adm;
import com.quanta.persistence.DAOAdmin;
import com.quanta.persistence.impl.DAOAdminImpl;

/**
 * Servlet implementation class DeleteUserAdmServlet
 */
@WebServlet("/DeleteUserAdmServlet")
public class DeleteUserAdmServlet extends HttpServlet {
	DAOAdmin dAOAdmin = new DAOAdminImpl();
	List<Adm> listUsers = new ArrayList<Adm>();
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uId = Integer.parseInt(request.getParameter("idUtente"));
		dAOAdmin.DeleteUser(uId);
		listUsers = dAOAdmin.FindAllUsers();
		request.setAttribute("listUsers", listUsers);
		request.setAttribute("aName", request.getSession().getAttribute("aName"));
		RequestDispatcher rd = request.getRequestDispatcher("listaUtenti.jsp");
		rd.forward(request, response);
	}

}
