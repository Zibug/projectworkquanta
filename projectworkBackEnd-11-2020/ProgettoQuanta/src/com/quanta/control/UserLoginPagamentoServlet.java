package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Adm;
import com.quanta.model.Cat;
import com.quanta.persistence.DAOAdmin;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOAdminImpl;
import com.quanta.persistence.impl.DAOCatImpl;

/**
 * Servlet implementation class UserLoginPagamentoServlet
 */
@WebServlet("/UserLoginPagamentoServlet")
public class UserLoginPagamentoServlet extends HttpServlet {
	DAOAdmin dAOAdmin = new DAOAdminImpl();
	List<Adm> uList = new ArrayList<Adm>();
	DAOCat dAOCat = new DAOCatImpl();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String aName = request.getParameter("aName");
		String aPsw = request.getParameter("aPsw");
		uList = dAOAdmin.FindUser(aName);
		Adm user = null;
		for(Adm usr : uList) {
			if(aPsw.equals(usr.getaPsw())) {
				user = usr;
			}
		}
		if(user != null) {
			boolean login = true;
			String registrazioneAvvenuta = "false";
			request.getSession().setAttribute("idUser", user.getaId());
			request.getSession().setAttribute("login", login);
			request.getSession().setAttribute("userName", user.getaName());
			request.setAttribute("userName", user.getaName());
			request.setAttribute("registrazioneAvvenuta", registrazioneAvvenuta);
			RequestDispatcher rd = request.getRequestDispatcher("pagamento.jsp");
			rd.forward(request, response);
		} else {
			String erroreDatiRegistrazione = "false";
			String erroreDatiInseriti = "true";
			String passwordUguali = "true";
			request.setAttribute("passwordUguali", passwordUguali);
			request.setAttribute("erroreDatiRegistrazione", erroreDatiRegistrazione);
			request.setAttribute("erroreDatiInseriti", erroreDatiInseriti);
			RequestDispatcher rd = request.getRequestDispatcher("loginPagamento.jsp");
			rd.forward(request, response);
		}
	}

}
