package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOProdImpl;

/**
 * Servlet implementation class DeleteImmServlet
 */
@WebServlet("/DeleteImmServlet")
public class DeleteImmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	Prod prod = new Prod();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("pId"));
		prod = dAOProd.FindProd(id);
		request.setAttribute("prod", prod);
		request.setAttribute("pId", id);
		RequestDispatcher rd = request.getRequestDispatcher("elimImm.jsp");
		rd.forward(request, response);
	}


}
