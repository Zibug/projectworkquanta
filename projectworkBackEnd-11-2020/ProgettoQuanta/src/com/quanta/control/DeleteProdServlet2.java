package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOCatImpl;
import com.quanta.persistence.impl.DAOProdImpl;

/**
 * Servlet implementation class DeleteProdServlet
 */
@WebServlet("/DeleteProdServlet2")
public class DeleteProdServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	List<Prod> listProd = new ArrayList<Prod>();
	DAOCat dAOCat = new DAOCatImpl();
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean elimina = false;
		if(request.getParameter("elimina").equals("true")) {
			elimina = true;
		}
		if(elimina) {
			int pId = Integer.parseInt(request.getParameter("pId"));
			String cName = dAOCat.getCName(dAOProd.getCId(pId));
			int cId = dAOProd.getCId(pId);
			//imm � legato con foreign key a prod, vanno eliminati tutti le imm per 
			//eliminare il prod
			dAOProd.deleteAllImm(pId);
			//listProd = dAOProd.FindAll(dAOProd.getCId(pId));
			listProd = dAOProd.FindAll(cId);
			request.setAttribute("cId", cId);
			request.setAttribute("listProd", listProd);
			request.setAttribute("cName", cName);
			RequestDispatcher rd = request.getRequestDispatcher("prodotti.jsp");
			rd.forward(request, response);
		} else {
			int pId = Integer.parseInt(request.getParameter("pId"));
			int cId = dAOProd.getCId(pId);
			String cName = dAOCat.getCName(cId);
			listProd = dAOProd.FindAll(cId);
			request.setAttribute("cId", cId);
			request.setAttribute("listProd", listProd);
			request.setAttribute("cName", cName);
			RequestDispatcher rd = request.getRequestDispatcher("prodotti.jsp");
			rd.forward(request, response);
		}
		
		
		
	}

}
