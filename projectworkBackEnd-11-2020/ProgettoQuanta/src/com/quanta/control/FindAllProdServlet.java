package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOProdImpl;

/**
 * Servlet implementation class FindAllProdServlet
 */
@WebServlet("/FindAllProdServlet")
public class FindAllProdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	List<Prod> listProd = new ArrayList<Prod>();
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cId = Integer.parseInt(request.getParameter("id"));
		String cName = request.getParameter("cName");
		listProd = dAOProd.FindAll(cId);
		request.setAttribute("cId", cId);
		request.setAttribute("listProd", listProd);
		request.setAttribute("cName", cName);
		RequestDispatcher rd = request.getRequestDispatcher("prodotti.jsp");
		rd.forward(request, response);
	}

}

























