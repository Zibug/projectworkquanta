package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.UserCart;

/**
 * Servlet implementation class VerifyLoginServlet
 */
@WebServlet("/VerifyLoginServlet")
public class VerifyLoginServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if((boolean)request.getSession().getAttribute("login")) {
			String registrazioneAvvenuta = "false";
			request.setAttribute("registrazioneAvvenuta", registrazioneAvvenuta);
			String userName = (String)request.getSession().getAttribute("userName");
			request.setAttribute("userName", userName);
			RequestDispatcher rd = request.getRequestDispatcher("pagamento.jsp");
			rd.forward(request, response);
		} else {
			String erroreDatiInseriti = "false";
			String erroreDatiRegistrazione = "false";
			String passwordUguali = "true";
			request.setAttribute("erroreDatiInseriti", erroreDatiInseriti);
			request.setAttribute("erroreDatiRegistrazione", erroreDatiRegistrazione);
			request.setAttribute("passwordUguali", passwordUguali);
			RequestDispatcher rd = request.getRequestDispatcher("loginPagamento.jsp");
			rd.forward(request, response);
		}
	}

}
