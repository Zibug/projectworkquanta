package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Cat;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOCatImpl;

/**
 * Servlet implementation class FindAllCatServlet
 */
@WebServlet("/FindAllCatServlet")
public class FindAllCatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOCat dAOCat = new DAOCatImpl();
	List<Cat> listCat = new ArrayList<Cat>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		listCat = dAOCat.findAll();
		request.setAttribute("lista", listCat);
		request.setAttribute("aName", (String)request.getSession().getAttribute("aName"));
		RequestDispatcher rd = request.getRequestDispatcher("categorie.jsp");
		rd.forward(request, response);
		/*
		listCat = dAOCat.findAll();
		request.setAttribute("listCat", listCat);
		RequestDispatcher rd = request.getRequestDispatcher("categorie.jsp");
		rd.forward(request, response);
		System.out.println("test");
		*/
	}
}
