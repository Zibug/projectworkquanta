package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Cat;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOCatImpl;

/**
 * Servlet implementation class EditProdServlet2
 */
@WebServlet("/EditCatServlet2")
public class EditCatServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOCat dAOCat = new DAOCatImpl();
	List<Cat> listCat = new ArrayList<Cat>();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cId = Integer.parseInt(request.getParameter("cId"));
		String cNome = dAOCat.getCName(cId);
		if(request.getParameter("cNome").isEmpty()) {
			boolean errore = true;
			request.setAttribute("errore", errore);
			request.setAttribute("cId", cId);
			request.setAttribute("cNome", cNome);
			RequestDispatcher rd = request.getRequestDispatcher("editCat.jsp");
			rd.forward(request, response);
		} else {
			dAOCat.UpdateCat(cId, request.getParameter("cNome"));
			listCat = dAOCat.findAll();
			request.setAttribute("lista", listCat);
			RequestDispatcher rd = request.getRequestDispatcher("categorie.jsp");
			rd.forward(request, response);
		}
		
	}

}
