package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.UserCred;
import com.quanta.persistence.DAOUserCred;
import com.quanta.persistence.impl.DAOUserCredImpl;

/**
 * Servlet implementation class FindUserServlet
 */
@WebServlet("/FindUserServlet")
public class FindUserServlet extends HttpServlet {
	UserCred userCred = new UserCred();
	DAOUserCred dAOUserCred = new DAOUserCredImpl();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idUtente = (Integer)request.getSession().getAttribute("idUser");
		userCred = dAOUserCred.FindById(idUtente);
		boolean errore = false;
		request.setAttribute("errore", errore);
		request.setAttribute("user", userCred);
		request.setAttribute("idUtente", idUtente);
		RequestDispatcher rd = request.getRequestDispatcher("userInfo.jsp");
		rd.forward(request, response);
	}

}
