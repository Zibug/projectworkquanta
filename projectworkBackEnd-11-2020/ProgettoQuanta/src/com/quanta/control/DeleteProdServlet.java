package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteProdServlet
 */
@WebServlet("/DeleteProdServlet")
public class DeleteProdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//int id = Integer.parseInt(request.getParameter("pId"));
		String id = request.getParameter("pId");
		request.setAttribute("pId", id);
		RequestDispatcher rd = request.getRequestDispatcher("deleteProd.jsp");
		rd.forward(request, response);
	}

}
