package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOProdImpl;
import com.quanta.model.Imm;
import java.util.List;

/**
 * Servlet implementation class AddImmServlet
 */
@WebServlet("/AddImmServlet")
public class AddImmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	Prod prod = new Prod();
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getParameter("url");
		int id = Integer.parseInt(request.getParameter("pId"));
		boolean pri = false;
		List<Imm> listImm = dAOProd.FindAllImm(id);
		if(listImm.size() == 0){
			pri = true;
		}
		dAOProd.addImm(url, pri, id);
		prod = dAOProd.FindProd(id);
		boolean errore = false;
		request.setAttribute("errore", errore);
		request.setAttribute("prod", prod);
		request.setAttribute("pId", id);
		RequestDispatcher rd = request.getRequestDispatcher("editProd.jsp");
		rd.forward(request, response);
	}

}




























