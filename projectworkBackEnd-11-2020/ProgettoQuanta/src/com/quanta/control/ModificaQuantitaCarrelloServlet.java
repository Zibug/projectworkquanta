package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.model.ProdCart;
import com.quanta.model.UserCart;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOProdImpl;

/**
 * Servlet implementation class ModificaQuantitaCarrelloServlet
 */
@WebServlet("/ModificaQuantitaCarrelloServlet")
public class ModificaQuantitaCarrelloServlet extends HttpServlet {
	DAOProd dAOProd = new DAOProdImpl();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int posizioneProdotto = Integer.parseInt(request.getParameter("posizioneProdottoNellaLista"));
		int nuovaQuantita = Integer.parseInt(request.getParameter("quantita"));
		UserCart userCart = (UserCart)request.getSession().getAttribute("carrello");
		if(nuovaQuantita == 0) {
			userCart.getListProdCart().remove(posizioneProdotto);
		} else {
			userCart.getListProdCart().get(posizioneProdotto).setPcQuant(nuovaQuantita);
		}
		
		
		
		
		List<Prod> listaProdottiNelCarrello = new ArrayList<Prod>();
		for(ProdCart datiProdotto:userCart.getListProdCart()) {
			Prod prodotto = dAOProd.FindProd(datiProdotto.getpId());
			prodotto.setpQuant(datiProdotto.getPcQuant());
			listaProdottiNelCarrello.add(prodotto);
		}
		request.setAttribute("listaProdottiNelCarrello", listaProdottiNelCarrello);
		boolean login = (boolean)request.getSession().getAttribute("login");
		request.setAttribute("login", login);
		RequestDispatcher rd = request.getRequestDispatcher("carrello.jsp"); 
		rd.forward(request, response);
		
	}

}
