package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Adm;
import com.quanta.model.Indirizzo;
import com.quanta.model.UserCred;
import com.quanta.persistence.DAOAdmin;
import com.quanta.persistence.DAOUserCred;
import com.quanta.persistence.impl.DAOAdminImpl;
import com.quanta.persistence.impl.DAOUserCredImpl;

/**
 * Servlet implementation class EditUserServlet
 */
@WebServlet("/EditUserServlet")
public class EditUserServlet extends HttpServlet {
	DAOUserCred dAOUser = new DAOUserCredImpl();
	DAOAdmin dAOAdmin = new DAOAdminImpl();
	List<Adm> listUsers = new ArrayList<Adm>();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idUtente = Integer.parseInt(request.getParameter("idUtente"));
		request.setAttribute("idUtente", idUtente);
		if(request.getParameter("uName").isEmpty() || request.getParameter("uPsw").isEmpty() || request.getParameter("nome").isEmpty() || request.getParameter("cognome").isEmpty() || request.getParameter("via").isEmpty() || request.getParameter("num").isEmpty() || request.getParameter("cap").isEmpty() || request.getParameter("paese").isEmpty()) {
			boolean errore = true;
			request.setAttribute("errore", errore);
			RequestDispatcher rd = request.getRequestDispatcher("userInfo.jsp");
			rd.forward(request, response);
		} else {
			String uName = request.getParameter("uName");
			String uPsw = request.getParameter("uPsw");
			String nome = request.getParameter("nome");
			String cognome = request.getParameter("cognome");
			String via = request.getParameter("via");
			int num = Integer.parseInt(request.getParameter("num"));
			int cap = Integer.parseInt(request.getParameter("cap"));
			String paese = request.getParameter("paese");
			UserCred newUser = new UserCred();
			newUser.setaId(idUtente);
			newUser.setaName(uName);
			newUser.setaPsw(uPsw);
			newUser.setNome(nome);
			newUser.setCognome(cognome);
			Indirizzo indirizzo = new Indirizzo();
			newUser.setIndirizzo(indirizzo);
			newUser.getIndirizzo().setVia(via);
			newUser.getIndirizzo().setNum(num);
			newUser.getIndirizzo().setCap(cap);
			newUser.getIndirizzo().setPaese(paese);;
			dAOUser.EditUserCredByAdm(newUser);
			listUsers = dAOAdmin.FindAllUsers();
			request.setAttribute("listUsers", listUsers);
			boolean errore = false;
			request.setAttribute("errore", errore);
			UserCred user = dAOUser.FindById(idUtente);
			request.setAttribute("user", user);
			RequestDispatcher rd = request.getRequestDispatcher("userInfo.jsp");
			rd.forward(request, response);
		}
	}

}
