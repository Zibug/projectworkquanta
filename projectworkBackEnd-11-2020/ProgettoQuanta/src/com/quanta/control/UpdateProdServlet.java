package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Prod;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.impl.DAOCatImpl;
import com.quanta.persistence.impl.DAOProdImpl;

/**
 * Servlet implementation class UpdateProdServlet
 */
@WebServlet("/UpdateProdServlet")
public class UpdateProdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOProd dAOProd = new DAOProdImpl();
	DAOCat dAOCat = new DAOCatImpl();  
	List<Prod> listProd = new ArrayList<Prod>();
	Prod prod = new Prod();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("pName").isEmpty() || request.getParameter("pDesc").isEmpty() || request.getParameter("pQuant").isEmpty() || request.getParameter("pPrezzo").isEmpty() || request.getParameter("immagine") == null) {		
			int id = Integer.parseInt(request.getParameter("pId"));
			prod = dAOProd.FindProd(id);
			boolean errore = true;
			request.setAttribute("prod", prod);
			request.setAttribute("errore", errore);
			request.setAttribute("pId", id);
			RequestDispatcher rd = request.getRequestDispatcher("editProd.jsp");
			rd.forward(request, response);
		} else {
			int idProd = Integer.parseInt(request.getParameter("pId"));
			String nome = request.getParameter("pName").toLowerCase();
			String desc = request.getParameter("pDesc");
			int quant = Integer.parseInt(request.getParameter("pQuant"));
			int prezzo = Integer.parseInt(request.getParameter("pPrezzo"));
			int idImm = Integer.parseInt(request.getParameter("immagine"));
			dAOProd.UpdateProd(idProd, nome, desc, quant, prezzo, idImm);
			int cId = dAOProd.getCId(idProd);
			String cName = dAOCat.getCName(cId);
			listProd = dAOProd.FindAll(cId);
			request.setAttribute("cId", cId);
			request.setAttribute("listProd", listProd);
			request.setAttribute("cName", cName);
			RequestDispatcher rd = request.getRequestDispatcher("prodotti.jsp");
			rd.forward(request, response);
		}
	}

}












