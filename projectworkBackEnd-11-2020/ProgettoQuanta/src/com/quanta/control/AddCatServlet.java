package com.quanta.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddCatServlet
 */
@WebServlet("/AddCatServlet")
public class AddCatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean errore = false;
		request.setAttribute("errore", errore);
		RequestDispatcher rd = request.getRequestDispatcher("addCat.jsp");
		rd.forward(request, response);
	}

}
