package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.quanta.persistence.DAOCat;
import com.quanta.persistence.impl.DAOCatImpl;
import com.quanta.model.*;

/**
 * Servlet implementation class MainServlet
 */
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOCat dAOCat = new DAOCatImpl();
	List<Cat> listCat = new ArrayList<Cat>();
     
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		listCat = dAOCat.findAll();
		boolean login = false;
		String userName = null;
		request.getSession().setAttribute("userName", userName);
		request.getSession().setAttribute("login", login);
		request.setAttribute("login", login);
		request.setAttribute("listCat", listCat);
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	}

}
