package com.quanta.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quanta.model.Adm;
import com.quanta.persistence.DAOAdmin;
import com.quanta.persistence.impl.DAOAdminImpl;

/**
 * Servlet implementation class AdminListServlet
 */
@WebServlet("/AdminLoginServlet")
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAOAdmin dAOadmin = new DAOAdminImpl();
	List<Adm> aList = new ArrayList<Adm>();
       
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean login = false;
		request.getSession().setAttribute("login", login);
		String userName = null;
		request.getSession().setAttribute("userName", userName);
		int idUser = 0;
		request.getSession().setAttribute("idUser", idUser);
		String aName = request.getParameter("aName");
		String aPsw = request.getParameter("aPsw");
		aList = dAOadmin.FindAdmin(aName);
		Adm admin = null;
		for(Adm adm : aList) {
			if(aPsw.equals(adm.getaPsw())) {
				admin = adm;
			}
		}
		if(admin != null) {
			request.setAttribute("aName", aName);
			request.getSession().setAttribute("aName", aName);
			RequestDispatcher rd = request.getRequestDispatcher("gestioneAdmin.jsp");
			rd.forward(request, response);
		} else {
			String erroreDatiRegistrazione = "true";
			String erroreDatiInseriti = "true";
			String adminBoolean = "true";
			request.setAttribute("erroreDatiRegistrazione", erroreDatiRegistrazione);
			request.setAttribute("admin", adminBoolean);
			request.setAttribute("erroreDatiInseriti", erroreDatiInseriti);
			RequestDispatcher rd = request.getRequestDispatcher("admin.jsp");
			rd.forward(request, response);
		}
	}

}
