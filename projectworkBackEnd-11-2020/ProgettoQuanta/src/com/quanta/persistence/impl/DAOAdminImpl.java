package com.quanta.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.xml.crypto.Data;

import java.util.ArrayList;

import com.quanta.model.*;
import com.quanta.persistence.*;


public class DAOAdminImpl implements DAOAdmin{

	DAOUserCred dAOUserCred = new DAOUserCredImpl();
	DAOIndirizzo dAOIndirizzo = new DAOIndirizzoImpl();
	DAOProdCart dAOProdCart = new DAOProdCartImpl();
	DAOUserCart dAOUserCart = new DAOUserCartImpl();
	
	public List<Adm> FindAdmin(String user) {
		
		String sql = "select a.Acc, a.Psw from adm a where a.Acc = ? and a.Ad = 1";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Adm> listAcc = new ArrayList<Adm>();
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, user);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				String acc = resultSet.getString(1);
				String psw = resultSet.getString(2);
				Adm adm = new Adm(acc, psw);
				listAcc.add(adm);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
		return listAcc;
	}
	
	public List<Adm> FindUser(String user) {
		
		String sql = "select a.Acc, a.Psw, a.Id from adm a where a.Acc = ? and a.Ad = 0";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Adm> listAcc = new ArrayList<Adm>();
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, user);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				String acc = resultSet.getString(1);
				String psw = resultSet.getString(2);
				int id = resultSet.getInt(3);
				Adm adm = new Adm();
				adm.setaId(id);
				adm.setaName(acc);
				adm.setaPsw(psw);
				listAcc.add(adm);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
		return listAcc;
	}
	
	public void InsertUser(UserCred userCred) {
		String sql = "insert into adm(Acc, Psw, ad) values( ?, ?, false)";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"Id"});
			statement.setString(1, userCred.getaName());
			statement.setString(2, userCred.getaPsw());
			statement.executeUpdate();
			ResultSet generetedKeys = statement.getGeneratedKeys();
			if(generetedKeys.next()) {
				userCred.setaId(generetedKeys.getInt(1));
				dAOUserCred.InsertUserCred(userCred);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
 	}
	
	public List<Adm> FindAllUsers(){
		String sql = "select * from adm where ad = false";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Adm> listUsers = new ArrayList<Adm>();
		try {
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				Adm adm = new Adm();
				adm.setaId(resultSet.getInt(1));
				adm.setaName(resultSet.getString(2));
				adm.setaPsw(resultSet.getString(3));
				listUsers.add(adm);
			}	
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return listUsers;
	}

	public void DeleteUser(int uId) {
		//dAOUserCart.delete ---> dAOProdCart.delete
		dAOUserCart.Delete(uId);
		//dAOUserCred.delete ---> dAOIndirizzo.delete
		dAOUserCred.Delete(uId);
		String sql = "delete from adm where Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, uId);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
	}
}
