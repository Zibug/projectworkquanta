package com.quanta.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.quanta.model.UserCart;
import com.quanta.persistence.DAOProdCart;
import com.quanta.persistence.DAOUserCart;
import com.quanta.persistence.DataSource;

public class DAOUserCartImpl implements DAOUserCart{
	DAOProdCart dAOProdCart = new DAOProdCartImpl();
	
	public void AddUserCart(UserCart userCart, int uId) {
		String sql = "insert into userCart(user_id) values(?)";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"Id"});
			statement.setInt(1, uId);
			statement.executeUpdate();
			ResultSet generetedKeys = statement.getGeneratedKeys();
			if(generetedKeys.next()) {
				userCart.setId(generetedKeys.getInt(1));
			}
			for(int i = 0; i < userCart.getListProdCart().size(); i++	) {
				dAOProdCart.AddProdCart(userCart.getListProdCart().get(i), userCart.getId());
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
	}
	
	public List<UserCart> FindAllByuId(int uId){
		String sql = "select * from usercart where user_id = ?";
		List<UserCart> listOrdini = new ArrayList<UserCart>();
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, uId);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				UserCart ordine = new UserCart();
				ordine.setId(resultSet.getInt(1));
				ordine.setListProdCart(dAOProdCart.FindAllByucId(ordine.getId()));
				listOrdini.add(ordine);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return listOrdini;
	}

	public void Delete(int uId) {
		//dAOProdCart.delete
		List<Integer> listaIdOrdini = FindIdByUId(uId);
		for(int i:listaIdOrdini) {
			dAOProdCart.Delete(i);
		}
		String sql = "delete from userCart where user_Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, uId);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
	
	public List<Integer> FindIdByUId(int uId) {
		String sql = "select userCart.Id from userCart where user_id = ?";
		List<Integer> id = new ArrayList<Integer>();
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, uId);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				id.add(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return id;
	}
}
