package com.quanta.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.quanta.model.Cat;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.DataSource;
import com.mysql.cj.x.protobuf.MysqlxPrepare.Prepare;
import com.quanta.model.*;

public class DAOCatImpl implements DAOCat{
	
	@Override
	public List<Cat> findAll() {
		String sql = "select * from cat";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet  = null;
		List<Cat> lista = new ArrayList<Cat>();
		try {
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				int cId = resultSet.getInt(1);
				String cNome = resultSet.getString(2);
				Cat cat = new Cat(cId, cNome);
				lista.add(cat);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return lista;
	}
	
	public String getCName(int cId) {
		String sql = "select c.Nome from cat c where c.Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String cName = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, cId);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				cName = resultSet.getString(1);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return cName;
	}

	public void UpdateCat(int cId, String cNome) {
		String sql = "update cat set Nome = ? where Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, cNome);
			statement.setInt(2, cId);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
	}
	
	public void DeleteCat(int id) {
		String sql = "delete from cat where Id = ?";
		Connection connection =  DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}

	public void AddCat(String cNome) {
		String sql = "insert into cat(Nome) values(?)";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"Id"});
			statement.setString(1, cNome);
			statement.executeUpdate();
			ResultSet generetedKeys = statement.getGeneratedKeys();
			int pId = 0;
			if(generetedKeys.next()) {
				pId = generetedKeys.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
	}
}
