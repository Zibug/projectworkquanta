package com.quanta.persistence.impl;

import java.io.DataOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.xdevapi.PreparableStatement;
import com.quanta.model.Imm;
import com.quanta.model.Prod;
import com.quanta.persistence.DAOCat;
import com.quanta.persistence.DAOProd;
import com.quanta.persistence.DataSource;
import com.sun.net.httpserver.Authenticator.Result;

public class DAOProdImpl implements DAOProd {
	
	DAOCat dAOCat = new DAOCatImpl();

	public List<Prod> FindAll(){
		String sql = "select p.Id, p.Nome, p.Quant, p.Des, p.Prezzo from prod p";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Prod> listProd = new ArrayList<Prod>();
		try {
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				int pId = resultSet.getInt(1);
				String pNome = resultSet.getString(2);
				int pQuant = resultSet.getInt(3);
				String pDes = resultSet.getString(4);
				int pPrezzo = resultSet.getInt(5);
				List<Imm> listaImm = FindAllImm(pId);
				Prod prod = new Prod(pId, pNome, pQuant, pPrezzo, pDes, listaImm);
				listProd.add(prod);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
		return listProd;
	}
	
	public List<Prod> FindAll(int id) {
		String sql = "select p.Id, p.Nome, p.Quant, p.Des, p.Prezzo from prod p where cat_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Prod> listProd = new ArrayList<Prod>();
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				int pId = resultSet.getInt(1);
				String pNome = resultSet.getString(2);
				int pQuant = resultSet.getInt(3);
				String pDes = resultSet.getString(4);
				int pPrezzo = resultSet.getInt(5);
				List<Imm> listaImm = FindAllImm(pId);
				Prod prod = new Prod(pId, pNome, pQuant, pPrezzo, pDes, listaImm);
				listProd.add(prod);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
		return listProd;
	}
	
	public List<Imm> FindAllImm(int id){
		String sql = "select i.Url, i.Pri, i.Id from imm i where prod_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Imm> listImm = new ArrayList<Imm>();
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				String url = resultSet.getString(1);
				boolean pri = resultSet.getBoolean(2);
				int idImm = resultSet.getInt(3);
				Imm imm = new Imm(idImm, url, pri);
				listImm.add(imm);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return listImm;
	}
	
	public Prod FindProd(int id) {
		String sql = "select p.Id, p.Nome, p.Quant, p.Des, p.Prezzo from prod p where p.Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Prod prod = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				int idProd = resultSet.getInt(1);
				String nome = resultSet.getString(2);
				int quant = resultSet.getInt(3);
				String desc = resultSet.getString(4);
				int prezzo = resultSet.getInt(5);
				List<Imm> imm = FindAllImm(id);
				prod = new Prod(idProd, nome, quant, prezzo, desc, imm);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return prod;
	}
	
	public void UpdateProd(int idProd, String nome, String desc, int quant, int prezzo, int idImm) {
		String sql = "update prod p set p.Nome = ?, p.Des = ?, p.Quant = ?, p.Prezzo = ? where p.Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, nome);
			statement.setString(2, desc);
			statement.setInt(3, quant);
			statement.setInt(4, prezzo);
			statement.setInt(5, idProd);
			statement.executeUpdate();
			UpdateImm(idImm, idProd);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
	}
	
	public void UpdateImm(int id, int idProd) {
		String sql = "update imm i set i.Pri = false where prod_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, idProd);
			statement.executeUpdate();
			UpdateImm2(id);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
		
	}
	
	public void UpdateImm2(int id) {
		String sql = "update imm i set i.Pri = true where i.Id = ?";
		System.out.println(sql);
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
	
	public int getCId(int pId) {
		String sql = "select p.cat_id from prod p where p.Id = ?;";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		int cId = 0;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, pId);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				cId = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
		return cId;
	}
	
	public void addImm(String url, boolean pri, int pId) {
		String sql = "insert into imm(Url, Pri, prod_id) values( ?, ?, ?);";
		Connection connection  = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"Id"});
			statement.setString(1, url);
			statement.setBoolean(2, pri);
			statement.setInt(3, pId);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}

	public void deleteImm(int id) {
		String sql = "delete from imm where Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
	
	public void deleteProd(int id) {
		String sql = "delete from prod p where p.Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}

	public void deleteAllImm(int id) {
		String sql = "delete from imm i where i.prod_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null; 
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
			deleteProd(id);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}

	public void addProd(Prod prod, int cId, String url) {
		String sql = "insert into prod(Nome, Quant, Prezzo, Des, cat_id) values( ?, ?, ?, ?, ?)";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"Id"});
			statement.setString(1, prod.getpName());
			statement.setInt(2, prod.getpQuant());
			statement.setInt(3, prod.getpPrezzo());
			statement.setString(4, prod.getpDesc());
			statement.setInt(5, cId);
			statement.executeUpdate();
			ResultSet generetedKeys = statement.getGeneratedKeys();
			int pId = 0;
			if(generetedKeys.next()) {
				pId = generetedKeys.getInt(1);
			}
			addImm(url, true, pId);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}

	public String FindImmUrl(int pId) {
		String sql = "select imm.Url from imm where prod_id = ? and Pri = true";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String url = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, pId);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				url = resultSet.getString(1);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return url;
	}
}




























