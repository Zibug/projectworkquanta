package com.quanta.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.quanta.model.UserCred;
import com.quanta.persistence.DAOIndirizzo;
import com.quanta.persistence.DataSource;


public class DAOIndirizzoImpl implements DAOIndirizzo{
	public void InsertIndirizzo(UserCred userCred) {
		String sql = "Insert into ind(via, num, cap, pae, userCred_id) values( ?, ?, ?, ?, ?)";
		Connection connection  = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"Id"});
			statement.setString(1, userCred.getIndirizzo().getVia());
			statement.setInt(2, userCred.getIndirizzo().getNum());
			statement.setInt(3, userCred.getIndirizzo().getCap());
			statement.setString(4, userCred.getIndirizzo().getPaese());
			statement.setInt(5, userCred.getId());
			statement.executeUpdate();
			ResultSet generatedKeys = statement.getGeneratedKeys();
			if(generatedKeys.next()) {
				//non � necessario per ora
				userCred.getIndirizzo().setId(generatedKeys.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
			
		}
		
	}

	public void Delete(int id){
		String sql = "delete from ind where userCred_Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
}
