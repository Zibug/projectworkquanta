package com.quanta.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.x.protobuf.MysqlxConnection.Close;
import com.quanta.model.Indirizzo;
import com.quanta.model.UserCred;
import com.quanta.persistence.DAOAdmin;
import com.quanta.persistence.DAOIndirizzo;
import com.quanta.persistence.DAOUserCred;
import com.quanta.persistence.DataSource;

public class DAOUserCredImpl implements DAOUserCred{
	
	DAOIndirizzo dAOIndirizzo = new DAOIndirizzoImpl();
	//DAOAdmin dAOAdmin = new DAOAdminImpl();
	
	public void InsertUserCred(UserCred userCred) {
		String sql = "insert into UserCred(Nome, Sur, user_id) values( ?, ?, ?)";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"Id"});
			statement.setString(1, userCred.getNome());
			statement.setString(2, userCred.getCognome());
			statement.setInt(3, userCred.getaId());
			statement.executeUpdate();
			ResultSet generatedKeys = statement.getGeneratedKeys();
			if(generatedKeys.next()) {
				userCred.setId(generatedKeys.getInt(1));
				dAOIndirizzo.InsertIndirizzo(userCred);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
			
		}
		
	}
	
	public UserCred FindById(int id) {
		UserCred userCred = new UserCred();
		String sql = "select a.Id, a.Acc, a.Psw, a.Ad, uc.Id, uc.nome, uc.sur, i.id, i.via, i.num, i.cap, i.pae from adm a inner join userCred uc on a.Id = uc.user_id inner join ind i on uc.Id = i.userCred_id where a.Ad = false and a.Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				userCred.setaId(resultSet.getInt(1));
				userCred.setaName(resultSet.getString(2));
				userCred.setaPsw(resultSet.getString(3));
				userCred.setAd(resultSet.getBoolean(4));
				userCred.setId(resultSet.getInt(5));
				userCred.setNome(resultSet.getString(6));
				userCred.setCognome(resultSet.getString(7));
				Indirizzo indirizzo = new Indirizzo();
				userCred.setIndirizzo(indirizzo);
				userCred.getIndirizzo().setId(resultSet.getInt(8));
				userCred.getIndirizzo().setVia(resultSet.getString(9));
				userCred.getIndirizzo().setNum(resultSet.getInt(10));
				userCred.getIndirizzo().setCap(resultSet.getInt(11));
				userCred.getIndirizzo().setPaese(resultSet.getString(12));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return userCred;
		
	}
	public void EditUserCredByAdm(UserCred newUser) {
		String sql = "update userCred set Nome = ?, Sur = ? where user_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, newUser.getNome());
			statement.setString(2, newUser.getCognome());
			statement.setInt(3, newUser.getaId());
			statement.executeUpdate();
			EditIndirizzoUserByAdm(newUser);
			EditUserByAdm(newUser);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
	
	public void EditIndirizzoUserByAdm(UserCred newUser){
		String sql = "update ind set via = ?, num = ?, cap = ?, pae = ? where userCred_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, newUser.getIndirizzo().getVia());
			statement.setInt(2, newUser.getIndirizzo().getNum());
			statement.setInt(3, newUser.getIndirizzo().getCap());
			statement.setString(4, newUser.getIndirizzo().getPaese());
			statement.setInt(5, newUser.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			 System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		
	}
	
	public void EditUserByAdm(UserCred userCred) {
		String sql = "update adm set Acc = ?, Psw = ? where Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, userCred.getaName());
			statement.setString(2, userCred.getaPsw());
			statement.setInt(3, userCred.getaId());
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
	
	public void Delete(int uId) {
		int id = FindIdByUId(uId);
		dAOIndirizzo.Delete(id);
		String sql = "delete from userCred where user_Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, uId);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
	
	public int FindIdByUId(int uId) {
		String sql = "select userCred.Id from userCred where user_id = ?";
		int id = 0;
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, uId);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				id = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return id;
	}
}
