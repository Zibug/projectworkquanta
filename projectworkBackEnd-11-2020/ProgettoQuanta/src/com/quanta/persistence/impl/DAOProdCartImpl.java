package com.quanta.persistence.impl;

import com.quanta.persistence.DAOProdCart;
import com.quanta.persistence.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.crypto.Data;

import com.mysql.cj.jdbc.AbandonedConnectionCleanupThread;
import com.quanta.model.*;

public class DAOProdCartImpl implements DAOProdCart {
	
	public void AddProdCart(ProdCart prodCart, int ucId) {
		String sql = "insert into prodCart(quant, prod_id, uca_id) values( ?, ?, ?)";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql, new String[] {"id"});
			statement.setInt(1, prodCart.getPcQuant());
			statement.setInt(2, prodCart.getpId());
			statement.setInt(3, ucId);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
	
	public List<ProdCart> FindAllByucId(int ucId){
		String sql = "select * from prodcart where uca_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<ProdCart> listProdCart = new ArrayList<ProdCart>();
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, ucId);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				int pcId = resultSet.getInt(1);
				int pcQuant = resultSet.getInt(2);
				int pId = resultSet.getInt(3);
				ProdCart prodottoCarrello = new ProdCart();
				prodottoCarrello.setPcId(pcId);
				prodottoCarrello.setPcQuant(pcQuant);
				prodottoCarrello.setpId(pId);
				listProdCart.add(prodottoCarrello);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(resultSet);
			DataSource.close(statement);
			DataSource.close(connection);
		}
		return listProdCart;
	}
	
	public void UpdateProdCart(ProdCart prodCart) {
		String sql = "update prodCart pc set pc.Nome = ?,  pc.Quant = ?,  pc.Prezzo = ?, pc.Des = ?, pc.prod_id = ?, pc.url_imm = ? where pc.Id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		List<ProdCart> listProdCart = new ArrayList<ProdCart>();
	}

	public void Delete(int cId) {
		String sql = "delete from prodCart where uca_id = ?";
		Connection connection = DataSource.getInstance().getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, cId);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			DataSource.close(statement);
			DataSource.close(connection);
		}
	}
}

























