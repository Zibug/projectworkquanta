package com.quanta.persistence;

import java.util.List;

import com.quanta.model.ProdCart;

public interface DAOProdCart {
	public void AddProdCart(ProdCart prodCart, int ucId);
	public void UpdateProdCart(ProdCart prodCart);
	public List<ProdCart> FindAllByucId(int ucId);
	public void Delete(int cId);
}
