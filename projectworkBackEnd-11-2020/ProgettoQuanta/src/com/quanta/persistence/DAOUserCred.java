package com.quanta.persistence;

import com.quanta.model.UserCred;

public interface DAOUserCred {
	public void InsertUserCred(UserCred userCred);
	public UserCred FindById(int id);
	public void EditUserCredByAdm(UserCred newUser);
	public void Delete(int uId);
}
