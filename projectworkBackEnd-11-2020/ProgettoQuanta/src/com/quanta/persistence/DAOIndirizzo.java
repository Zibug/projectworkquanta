package com.quanta.persistence;

import com.quanta.model.UserCred;

public interface DAOIndirizzo {
	public void InsertIndirizzo(UserCred userCred);
	public void Delete(int id);
}
