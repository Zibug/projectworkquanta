package com.quanta.persistence;
import java.util.List;

import com.quanta.model.*;

public interface DAOAdmin {
	public List<Adm> FindAdmin(String user);
	List<Adm> FindUser(String user);
	public void InsertUser(UserCred userCred);
	public List<Adm> FindAllUsers();
	public void DeleteUser(int uId);
}
