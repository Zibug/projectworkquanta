package com.quanta.persistence;

import java.util.List;

import com.quanta.model.Adm;
import com.quanta.model.Cat;

public interface DAOCat {
	public List<Cat> findAll();
	public String getCName(int cId);
	public void DeleteCat(int id);
	public void UpdateCat(int cId, String cNome);
	public void AddCat(String cNome);
}
