package com.quanta.persistence;

import java.util.List;

import com.quanta.model.Imm;
import com.quanta.model.Prod;

public interface DAOProd {
	//Trova tutti i prodotti
	public List<Prod> FindAll();
	//Trova tutti i prodotti di una categoria 
	public List<Prod> FindAll(int id);
	public List<Imm> FindAllImm(int id);
	public Prod FindProd(int id);
	public void UpdateProd(int idProd, String nome, String desc, int quant, int prezzo, int idImm);
	public void UpdateImm(int id, int idProd);
	public int getCId(int pId);
	public void addImm(String url, boolean pri, int pId);
	public void deleteImm(int id);
	public void deleteProd(int id);
	public void deleteAllImm(int id);
	public void addProd(Prod prod, int cId, String url);
	public String FindImmUrl(int pId);
}
