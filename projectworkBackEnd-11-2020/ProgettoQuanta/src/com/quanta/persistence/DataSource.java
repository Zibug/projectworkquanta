package com.quanta.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataSource {

	private static DataSource instance;

	// private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DRIVER = "com.mysql.jdbc.Driver"; //nome del driver utilizzato
	//stringa di connessione
	//jdbc � il protocollo:mysql � il nome del dbms, servce al driver manager per capire con quale driver si vuole lavorare: ip: porta: database: 
	private static final String URL = "jdbc:mysql://127.0.0.1:3306/projectwork?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useTimezone=true&serverTimezone=UTC";
	private static final String USER = "root";
	private static final String PASSWORD = "admin";

	static {
		try {
			// registra il driver al driver manager
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public static DataSource getInstance() {
		if (instance == null) {
			instance = new DataSource();
		}
		return instance;
	}

	private DataSource() {

	}
	
	//connessione
	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(URL, USER, PASSWORD); //da errore di tipo checked
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return connection;
	}

	//chiusura connessione
	public static void close(Connection connection) { //tutti gli if sull null servono per evitare gli errori 
													  //unchecked di nullpointer
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public static void close(Statement statement) { //anche statement � un  interfaccia
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public static void close(ResultSet resultSet) { //result set conterra le righe di output dei vari select
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
	}

}