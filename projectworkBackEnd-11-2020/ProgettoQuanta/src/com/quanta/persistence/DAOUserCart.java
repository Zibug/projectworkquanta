package com.quanta.persistence;

import java.util.List;

import com.quanta.model.UserCart;

public interface DAOUserCart {
	public void AddUserCart(UserCart userCart, int uId);
	public List<UserCart> FindAllByuId(int uId);
	public void Delete(int uId);
}
