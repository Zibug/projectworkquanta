package com.quanta.model;

public class ProdCart {
	private int pcId;
	private int pcQuant;
	private int pId;
	private int uca_id;
	public ProdCart() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProdCart(int pcId, int pcQuant, int pId, int uca_id) {
		super();
		this.pcId = pcId;
		this.pcQuant = pcQuant;
		this.pId = pId;
		this.uca_id = uca_id;
	}
	public int getPcId() {
		return pcId;
	}
	public void setPcId(int pcId) {
		this.pcId = pcId;
	}
	public int getPcQuant() {
		return pcQuant;
	}
	public void setPcQuant(int pcQuant) {
		this.pcQuant = pcQuant;
	}
	public int getpId() {
		return pId;
	}
	public void setpId(int pId) {
		this.pId = pId;
	}
	public int getUca_id() {
		return uca_id;
	}
	public void setUca_id(int uca_id) {
		this.uca_id = uca_id;
	}
	@Override
	public String toString() {
		return "ProdCart [pcId=" + pcId + ", pcQuant=" + pcQuant + ", pId=" + pId + ", uca_id=" + uca_id + "]";
	}

	
	
	
}