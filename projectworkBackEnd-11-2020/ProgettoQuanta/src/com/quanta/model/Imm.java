package com.quanta.model;

public class Imm extends Prod {
	private int iId;
	private String url;
	private boolean pri;
	
	public Imm() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Imm(String url, boolean pri) {
		super();
		this.url = url;
		this.pri = pri;
	}
	public Imm(int iId, String url, boolean pri) {
		super();
		this.iId = iId;
		this.url = url;
		this.pri = pri;
	}
	public int getiId() {
		return iId;
	}
	public void setiId(int iId) {
		this.iId = iId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isPri() {
		return pri;
	}
	public void setPri(boolean pri) {
		this.pri = pri;
	}
	
	
}
