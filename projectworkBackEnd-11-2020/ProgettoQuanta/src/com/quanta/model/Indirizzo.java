package com.quanta.model;

public class Indirizzo {
	private int id;
	private String via;
	private int num;
	private int cap;
	private String paese;
	
	public Indirizzo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Indirizzo(String via, int num, int cap, String paese) {
		super();
		this.via = via;
		this.num = num;
		this.cap = cap;
		this.paese = paese;
		
	}
	public Indirizzo(int id, String via, int num, int cap, String paese) {
		super();
		this.id = id;
		this.via = via;
		this.num = num;
		this.cap = cap;
		this.paese = paese;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getCap() {
		return cap;
	}
	public void setCap(int cap) {
		this.cap = cap;
	}
	public String getPaese() {
		return paese;
	}
	public void setPaese(String paese) {
		this.paese = paese;
	}
	@Override
	public String toString() {
		return "Indirizzo [id=" + id + ", via=" + via + ", num=" + num + ", cap=" + cap + ", paese=" + paese + "]";
	}
}
