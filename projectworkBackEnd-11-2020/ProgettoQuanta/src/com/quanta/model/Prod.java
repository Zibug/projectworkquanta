package com.quanta.model;

import java.util.ArrayList;
import java.util.List;

public class Prod {
	private int pId;
	private String pName;
	private int pQuant;
	private int pPrezzo;
	private String pDesc;
	private List<Imm> listaImm = new ArrayList<Imm>();
	
	public Prod() {
		super();
	}
	public Prod(int pId, String pName, int pQuant, int pPrezzo, String pDesc) {
		super();
		this.pId = pId;
		this.pName = pName;
		this.pQuant = pQuant;
		this.pPrezzo = pPrezzo;
		this.pDesc = pDesc;
	}
	public Prod(String pNome, String pDesc, int pQuant, int pPrezzo) {
		super();
		this.pName = pNome;
		this.pDesc = pDesc;
		this.pQuant = pQuant;
		this.pPrezzo = pPrezzo;
	}
	public Prod(int pId, String pName, int pQuant, int pPrezzo, String pDesc, List<Imm> listaImm) {
		super();
		this.pId = pId;
		this.pName = pName;
		this.pQuant = pQuant;
		this.pPrezzo = pPrezzo;
		this.pDesc = pDesc;
		this.listaImm = listaImm;
	}
	public int getpId() {
		return pId;
	}
	public void setpId(int pId) {
		this.pId = pId;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public int getpQuant() {
		return pQuant;
	}
	public void setpQuant(int pQuant) {
		this.pQuant = pQuant;
	}
	public int getpPrezzo() {
		return pPrezzo;
	}
	public void setpPrezzo(int pPrezzo) {
		this.pPrezzo = pPrezzo;
	}
	public String getpDesc() {
		return pDesc;
	}
	public void setpDesc(String pDesc) {
		this.pDesc = pDesc;
	}
	public List<Imm> getListaImm() {
		return listaImm;
	}
	public void setListaImm(List<Imm> listaImm) {
		this.listaImm = listaImm;
	}
	@Override
	public String toString() {
		return "Prod [pId=" + pId + ", pName=" + pName + ", pQuant=" + pQuant + ", pPrezzo=" + pPrezzo + ", pDesc="
				+ pDesc + ", listaImm=" + listaImm + "]";
	}
	
	
	

}
