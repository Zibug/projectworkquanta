package com.quanta.model;

import java.util.ArrayList;
import java.util.List;

public class UserCart {
	private int id;
	private List<ProdCart> listProdCart = new ArrayList<ProdCart>();
	private List<Prod> listProdPrint = new ArrayList<Prod>();
	public UserCart() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserCart(int id, List<ProdCart> listProdCart) {
		super();
		this.id = id;
		this.listProdCart = listProdCart;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<ProdCart> getListProdCart() {
		return listProdCart;
	}
	public void setListProdCart(List<ProdCart> listProdCart) {
		this.listProdCart = listProdCart;
	}
	public List<Prod> getListProdPrint() {
		return listProdPrint;
	}
	public void setListProdPrint(List<Prod> listProdPrint) {
		this.listProdPrint = listProdPrint;
	}
	@Override
	public String toString() {
		return "UserCart [id=" + id + ", listProdCart=" + listProdCart + "]";
	}
	
	
	
	

}
