package com.quanta.model;

public class Adm {
	private int aId;
	private String aName;
	private String aPsw;
	private boolean ad;
	
	public Adm() {
		super();
	}
	
	public Adm(String aName, String aPsw) {
		super();
		this.aName = aName;
		this.aPsw = aPsw;
	}

	public Adm(int aId, String aName, String aPsw, boolean ad) {
		super();
		this.aId = aId;
		this.aName = aName;
		this.aPsw = aPsw;
		this.ad = ad;
	}
	
	
	public int getaId() {
		return aId;
	}

	public void setaId(int aId) {
		this.aId = aId;
	}

	public String getaName() {
		return aName;
	}

	public void setaName(String aName) {
		this.aName = aName;
	}

	public String getaPsw() {
		return aPsw;
	}

	public void setaPsw(String aPsw) {
		this.aPsw = aPsw;
	}

	public boolean isAd() {
		return ad;
	}

	public void setAd(boolean ad) {
		this.ad = ad;
	}
	
	
}