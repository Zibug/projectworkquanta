package com.quanta.model;

import java.util.ArrayList;
import java.util.List;

public class Cat {
	private int cId;
	private String cName;
	private List<Prod> cListaProd = new ArrayList<Prod>();
	
	public Cat() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Cat(String cName) {
		super();
		this.cName = cName;
	}
	public Cat(int cId, String cName) {
		super();
		this.cId = cId;
		this.cName = cName;
	}
	public Cat(int cId, String cName, List<Prod> cListaProd) {
		super();
		this.cId = cId;
		this.cName = cName;
		this.cListaProd = cListaProd;
	}
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public List<Prod> getcListaProd() {
		return cListaProd;
	}
	public void setcListaProd(List<Prod> cListaProd) {
		this.cListaProd = cListaProd;
	}
	
	
	
}
