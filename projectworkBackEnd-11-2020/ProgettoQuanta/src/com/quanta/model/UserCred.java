package com.quanta.model;

public class UserCred extends Adm{
	private int id;
	private String nome;
	private String cognome;
	private Indirizzo indirizzo;
	
	public UserCred() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserCred(int aId, String aName, String aPsw, boolean ad) {
		super(aId, aName, aPsw, ad);
		// TODO Auto-generated constructor stub
	}
	public UserCred(String aName, String aPsw) {
		super(aName, aPsw);
		// TODO Auto-generated constructor stub
	}
	public UserCred(int id, String nome, String cognome, Indirizzo indirizzo) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.indirizzo = indirizzo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public Indirizzo getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}
	@Override
	public String toString() {
		return "UserCred [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", indirizzo=" + indirizzo + "]";
	}
	
}
