<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import = "com.quanta.model.Prod" %>
<%@ page import = "com.quanta.model.UserCart" %>
<%@ page import = "com.quanta.model.Imm" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
	String aName = null;
	if(request.getAttribute("aName") != null){
		aName = (String)request.getAttribute("aName");
	}
	String admState = (String)request.getAttribute("admState");
	int prezzoTotale = 0;
	List<UserCart> listaOrdini = (List<UserCart>)request.getAttribute("listaOrdini"); 
	if(admState.equals("true")){
	%>
		<a href = "gestioneAdmin.jsp?aName=<%=aName%>">Torna alla pagina dell'admin</a>
	<%
	} else {
	%>
	<a href = "main2">Torna alla Pagina Principale</a>
	<%
	}
	for(int i = 0; i < listaOrdini.size(); i++){
		UserCart listaDaStampare = listaOrdini.get(i);
	%>
		<table style = "border:2px; border-color:red; border-style:solid;border-width: 2px">
			<caption>Ordine codice: <%=listaDaStampare.getId() %></caption>
			<thead>
				<tr> 
					<th> Nome </th>
					<th> Immagine</th>
					<th> Descrizione</th>
					<th> Quantita </th>
					<th> Prezzo </th>
				</tr>
			</thead>
			<tbody>
				<% 
				for(int j = 0; j < listaDaStampare.getListProdPrint().size(); j++){
				%>
					<tr> 
						<td> 
						<%= listaDaStampare.getListProdPrint().get(j).getpName() %>
						</td>
						<td> 
						<% 
						List<Imm> listImm = listaDaStampare.getListProdPrint().get(j).getListaImm(); 
							if(listImm.size() == 0){
						%>
							Empty1
						<% 
						} else{
							if(listImm.get(0) == null){
						%>
									Empty2
							<%
							} else {
								Imm pri = null;
								for(Imm imm : listImm){
									if(imm.isPri()){
										pri = imm;
										%>
										<img src = "<%= pri.getUrl()%>" width="256" height="192">
										<%
									}
								}
								if(pri == null){
									%>
									<img src = "<%= listImm.get(0).getUrl()%>" width="256" height="192">
									<%
								}
							}
						}
						%>
						</td>
						<td>
							<%= listaDaStampare.getListProdPrint().get(j).getpDesc() %>
						</td>
						<td>
							<%= listaDaStampare.getListProdPrint().get(j).getpQuant() %>
						</td>
						<td>
							<%prezzoTotale = prezzoTotale +  listaDaStampare.getListProdPrint().get(j).getpPrezzo() * listaDaStampare.getListProdPrint().get(j).getpQuant();%>
							<%= listaDaStampare.getListProdPrint().get(j).getpPrezzo() * listaDaStampare.getListProdPrint().get(j).getpQuant() %>
						</td>
					</tr>
				<%
				} 
				%>
					<tr>
						<td style="font-weight: bold;font-size: x-large;">Prezzo totale: </td>
						<td style="font-weight: bold;font-size: x-large;"><%=prezzoTotale %> </td>
						<td> </td>
						<td> </td>
						<td> </td>
					</tr>
			</tbody>
		</table>
	<%	
	}
	%>
</body>
</html>