<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "com.quanta.model.*" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% String aName = (String)request.getAttribute("aName");%>
	<% List<Adm> listUsers = (ArrayList<Adm>)request.getAttribute("listUsers");%>
	<a href = "gestioneAdmin.jsp?aName=<%=aName%>">Torna alla pagina dell'admin</a>
	<table style = "border:2px; border-color:red; border-style:solid;border-width: 2px">
		<caption>Utenti</caption>
		<thead> 
			<tr> 
				<th> Nome </th>
			</tr>
		</thead>
		<tbody>
		<% 
		for(int i = 0; i < listUsers.size(); i++){
		%>
			<tr> 
				<td> 
				<%= listUsers.get(i).getaName() %>
				</td>
				<td> 
					<form method = "post" action="FindUserAdmServlet">
						<input type = "hidden" name = "idUtente" value = "<%= listUsers.get(i).getaId()%>">
						<input type = "submit" value = "Visualizza/Modifica">
					</form>
				</td>
				<td>
					<form method = "post" action="DeleteUserAdmServlet">
						<input type = "hidden" name = "idUtente" value = "<%= listUsers.get(i).getaId() %>">
						<input type = "submit" value = "Delete">
					</form>
				</td>
			</tr>
		<% } %>
		</tbody>
	</table>
</body>
</html>