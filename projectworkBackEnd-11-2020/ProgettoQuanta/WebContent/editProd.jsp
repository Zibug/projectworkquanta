<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "com.quanta.model.Prod" %>
<%@ page import = "java.util.List" %>
<%@ page import = "com.quanta.model.Imm" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%Prod prod = (Prod)request.getAttribute("prod"); %>
	<%boolean errore = (boolean)request.getAttribute("errore");%>
	<%
		if(errore){
	%>
			<p style="font-size: x-large; color: red">- Errore completare tutti i campi </p>
	<%
		}
	%>
	<%
		if(prod.getListaImm().size() == 0){
	%>
			<p style="font-size: x-large; color: red">- Aggiungere un'immagine </p>
	<%
		}
	%>
	<form action = "UpdateProdServlet">
		<p>Inserire le nuove caratteristiche:</p>
		<input type = "hidden" name = "pId" value = "<%= prod.getpId()%>">
		<label for = "nome">Nome: </label>
		<input type = "text" id = "nome" name = "pName" value = "<%= prod.getpName()%>">
		<br>
		<label for = "descrizione">Descrizione: </label>
		<input type = "text" id = "descrizione" name = "pDesc" value = "<%= prod.getpDesc()%>">
		<br>
		<label for = "quantita">Quantita: </label>
		<input type = "number" id = "quantita" name = "pQuant" value = "<%= prod.getpQuant()%>">
		<br>
		<label for = "prezzo">Prezzo: </label>
		<input type = "number" id = "prezzo" name = "pPrezzo" value = "<%= prod.getpPrezzo()%>">
		<br>
		<p> Scegliere l'immagine principale:</p>
		<% for(int i = 0; i < prod.getListaImm().size(); i++){ %>
			<input for = "imm<%=i %>" type = "radio" name = "immagine" value = "<%= prod.getListaImm().get(i).getiId()%>">
		 	<img id ="imm<%=i %>" src = "<%= prod.getListaImm().get(i).getUrl() %>" width="256" height="192">
			<br>
		<% } %>
		<input type = "submit" value = "Conferma">
	</form>
	<br>
	<form action = "DeleteImmServlet">
		<input type = "hidden" name = "pId" value = "<%= prod.getpId()%>">
		<input type = "submit" value = "Cliccare qui per eliminare un immagine" >
	</form>
	<form action = "AddImmServlet">
		<input type = "hidden" name = "pId" value = "<%= prod.getpId()%>">
		<label for = "url" >Inserisici URL nuova immagine da inserire: </label>
		<input type = "text" id = "url" name = "url">			
		<input type = "submit" value = "Aggiungi Immagine">
	</form>
	<a href = "FindAllCatServlet">Torna a categorie di prodotti</a>
</body>
</html>




























