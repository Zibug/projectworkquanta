<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "com.quanta.model.UserCred" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	<% boolean errore = (boolean)request.getAttribute("errore");%>
	<% int idUtente = (Integer)request.getAttribute("idUtente"); %>
	<%UserCred user = (UserCred)request.getAttribute("user"); %>
	<a href = "main2">Torna alla Pagina Principale</a>
	<br>
	Dati dell'utente: <%= user.getaName()%>
	<form method = "post" action="EditUserServlet">
		<% 
		if(errore){
		%>
			<p style = "color:red">Errore, riempire tutti i campi </p>
		<% 
		}
		%>
		<input type = "hidden" name = "idUtente" value = "<%=idUtente%>">
		<label for = "uName">Username: </label>
		<input type = "text" id = "uName" name = "uName" value = "<%= user.getaName() %>">
		<br>
		<label for = "uPsw">Password: </label>
		<input type = "text" id = "uPsw" name = "uPsw" value = "<%= user.getaPsw() %>">
		<br>
		<label for = "nome">Nome: </label>
		<input type = "text" id = "nome" name = "nome" value = "<%= user.getNome() %>">
		<br>
		<label for = "cognome">Congome: </label>
		<input type = "text" id = "cognome" name = "cognome" value = "<%= user.getCognome() %>">
		<br>
		<label for = "via">Via: </label>
		<input type = "text" id = "via" name = "via" value = "<%= user.getIndirizzo().getVia() %>">
		<br>
		<label for = "num">Numero: </label>
		<input type = "number" id = "num" name = "num" value = "<%= user.getIndirizzo().getNum() %>">
		<br>
		<label for = "cap">Cap: </label>
		<input type = "number" id = "cap" name = "cap" value = "<%= user.getIndirizzo().getCap() %>">
		<br>
		<label for = "paese">Paese: </label>
		<input type = "text" id = "paese" name = "paese" value = "<%= user.getIndirizzo().getPaese() %>">
		<br>
		<input type = "submit" value = "Conferma">
	</form>
	<a href = "FindAllOldOrderServlet?admState=false&aName=false">Visualizza Cronologia Ordini</a>
</body>
</html>