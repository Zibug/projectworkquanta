<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>GestionAdmin</title>
</head>
<body>
	<a href = "main2"> Torna alla Pagine Principale </a>
	<% 
		String aName;
		if(request.getAttribute("aName") != null){
		aName = (String)request.getAttribute("aName");
		} else {
			aName = request.getParameter("aName"); 
		}
	%>
	<p>Benvenuto 
	<%=  aName%>
	</p>
	<ul>
	<li><a href = "FindAllCatServlet">Mostra categorie di prodotti</a> </li>
	<li><a href = "FindAllUsers">Gestione Clienti</a> </li>
	</ul>
</body>			
</html>							