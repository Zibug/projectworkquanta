<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "com.quanta.model.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pagina Iniziale</title>
</head>
<body>
	<% boolean login = (boolean)request.getAttribute("login");%>
	<% List<Cat> listCat = (ArrayList<Cat>)request.getAttribute("listCat"); %>
	<% 
	String userName = null;
	if(request.getAttribute("userName") != null){
		userName = (String)request.getAttribute("userName");
	}
	%>
	<p>IndexPage </p>
	<a href = "admin.jsp?admin=true&erroreDatiInseriti=false&erroreDatiRegistrazione=false&passwordUguali=true">Pagina admin</a>
	<br>
	<%
	if(login && userName != null){
	%>
		Benvenuto<a href = "FindUserServlet"> <%= userName %>, </a>
		<br>
		<a href = "LogoutServlet">Logout</a> 
	<%
	} else{
	%>
		<a href = "admin.jsp?admin=false&erroreDatiInseriti=false&erroreDatiRegistrazione=false&passwordUguali=true">Login Utente</a>
		<br>
	<%
	}
	%>
	<br>
	<a href = "MostraCarrelloServlet">Mostra Carrello</a>
	<form action="FindProdServlet">
		<label for = "ricerca">Ricerca:</label>
		<input type = "text" id = "ricerca" name = "ricerca" placeholder = "es. computer">
		<input type = "submit" value = "Cerca">
	</form>
	<p>Categorie: </p>
	<ul>
		<%
		for(int i = 0; i < listCat.size(); i++){
		%>	
			<li><a href ="FindProdIndexServlet?cId=<%=listCat.get(i).getcId()%>"><%= listCat.get(i).getcName() %> </a></li> 
		<%
		}
		%>
	</ul>
</body>
</html>