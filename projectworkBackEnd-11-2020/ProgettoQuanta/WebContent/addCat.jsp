<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% boolean errore = (boolean)request.getAttribute("errore");%>
	<p>Inserire le caratteristiche della nuova categoria da inserire nel database:</p>
	<%
		if(errore){
	%>
			<p style="font-size: x-large; color: red">- Errore completare tutti i campi </p>
	<%		
		}
	%>
	<form method = "post" action = "AddCatServlet2">
		<label for = "nome">Nome: </label>
		<input type = "text" id = "nome" name = "cNome" placeholder = "es. cartoleria">
		<input type = "submit" value = "Conferma">
	</form>
		<br>
</body>
</html>