<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "com.quanta.model.Prod" %>
<%@ page import = "com.quanta.model.Imm" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% boolean login = (boolean)request.getAttribute("login"); %>
	<% int cId = (Integer)request.getAttribute("cId"); %>
	<% String cName = (String)request.getAttribute("cNome"); %>
	<% List<Prod> listProd = (ArrayList<Prod>)request.getAttribute("listProd");%>
	<a href = "main2">Torna alla Pagina principale</a>
	<p>Lista Prodotti Categoria <%= cName%></p>
	<table style = "border:2px; border-color:red; border-style:solid;border-width: 2px">
		<caption>Prodotti</caption>
		<thead> 
			<tr> 
				<th> Nome </th>
				<th> Immagine</th>
				<th> Descrizione</th>
				<th> Quantita </th>
				<th> Prezzo </th>
			</tr>
		</thead>
		<tbody>
		<% 
		for(int i = 0; i < listProd.size(); i++){
		%>
			<tr> 
				<td> 
				<%= listProd.get(i).getpName() %>
				</td>
				<td> 
				<% 
				List<Imm> listImm = listProd.get(i).getListaImm(); 
				if(listImm.size() == 0){
				%>
					Empty1
				<% 
				} else{
					if(listImm.get(0) == null){
						%>
							Empty2
						<%
					} else {
						Imm pri = null;
						for(Imm imm : listImm){
							if(imm.isPri()){
								pri = imm;
								%>
									<img src = "<%= pri.getUrl()%>" width="256" height="192">
								<%
						}
						}
						if(pri == null){
							%>
							<img src = "<%= listImm.get(0).getUrl()%>" width="256" height="192">
							<%
						}
					}
				}
				%>
				</td>
				<td>
					<%= listProd.get(i).getpDesc() %>
				</td>
				<td>
					<%= listProd.get(i).getpQuant() %>	
				</td>
				<td>
					<%= listProd.get(i).getpPrezzo() %>
				</td>
				<td>
					<form action="AddCartServlet">
						<input type = "hidden" name = "pId" value = "<%= listProd.get(i).getpId()%>" >
						<input type = "number" style="width: 50px;" min="0" name = "pcQuant" value = "1">
						<input type = "submit" value = "Aggiungi al carrello">
					</form>
				</td>
			</tr>
		<%} %>
		</tbody>
	</table>
</body>
</html>