<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% String registrazioneAvvenuta = (String)request.getAttribute("registrazioneAvvenuta"); %>
	<% String userName = (String)request.getAttribute("userName"); %>
	<a href = "main2">Torna alla pagina Pincipale</a>
	<% if(registrazioneAvvenuta.equals("true")){ %>
			<p>Registrazione Avvenuta con successo, ora puoi procedere con il pagamento <%= userName %> </p>
	<%} %>
	<form method = "post" action="SaveCartServlet">
		<input type = "submit" value = "Paga" >
	</form>
</body>
</html>