<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "com.quanta.model.UserCred" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% boolean errore = (boolean)request.getAttribute("errore");%>
	<% int idUtente = (Integer)request.getAttribute("idUtente"); %>
	<% String aName = (String)request.getAttribute("aName"); %>
	<% UserCred user = (UserCred)request.getAttribute("user"); %>
	<a href = "gestioneAdmin.jsp?aName=<%=aName%>">Torna alla pagina dell'admin</a>
	<br>
	<form method = "post" action="EditUserAdmServlet">
		<% 
		if(errore){
		%>
			<p style = "color:red">Errore, riempire tutti i campi </p>
		<% 
		}
		%>
		<input type = "hidden" name = "idUtente" value = "<%=idUtente%>">
		<label for = "uName">Username: </label>
		<input type = "text" id = "uName" name = "uName" value = "<%= user.getaName() %>">
		<br>
		<label for = "uPsw">Password: </label>
		<input type = "text" id = "uPsw" name = "uPsw" value = "<%= user.getaPsw() %>">
		<br>
		<label for = "nome">Nome: </label>
		<input type = "text" id = "nome" name = "nome" value = "<%= user.getNome() %>">
		<br>
		<label for = "cognome">Congome: </label>
		<input type = "text" id = "cognome" name = "cognome" value = "<%= user.getCognome() %>">
		<br>
		<label for = "via">Via: </label>
		<input type = "text" id = "via" name = "via" value = "<%= user.getIndirizzo().getVia() %>">
		<br>
		<label for = "num">Numero: </label>
		<input type = "number" id = "num" name = "num" value = "<%= user.getIndirizzo().getNum() %>">
		<br>
		<label for = "cap">Cap: </label>
		<input type = "number" id = "cap" name = "cap" value = "<%= user.getIndirizzo().getCap() %>">
		<br>
		<label for = "paese">Paese: </label>
		<input type = "text" id = "paese" name = "paese" value = "<%= user.getIndirizzo().getPaese() %>">
		<br>
		<input type = "submit" value = "Conferma">
	</form>
	<a href = "FindAllOldOrderServlet?admState=true&aName=<%= aName%>&idUser=<%=idUtente%>">Visualizza Cronologia Ordini</a>
</body>
</html>