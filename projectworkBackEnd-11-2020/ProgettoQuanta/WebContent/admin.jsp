<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% 
	String admin;
	if(request.getAttribute("admin") == null ){
		admin = request.getParameter("admin");
	} else {
		admin = (String)request.getAttribute("admin"); 
	}
	String erroreDatiInseriti; 
	if(request.getAttribute("erroreDatiInseriti") == null){
		erroreDatiInseriti = request.getParameter("erroreDatiInseriti"); 
	} else{
		erroreDatiInseriti = (String)request.getAttribute("erroreDatiInseriti");
	}
	String erroreDatiRegistrazione; 
	if(request.getAttribute("erroreDatiRegistrazione") == null){
		erroreDatiRegistrazione = request.getParameter("erroreDatiRegistrazione"); 
	} else{
		erroreDatiRegistrazione = (String)request.getAttribute("erroreDatiRegistrazione");
	}
	String passwordUguali;
	if(request.getAttribute("passwordUguali") == null ){
		passwordUguali = request.getParameter("passwordUguali");
	} else {
		passwordUguali = (String)request.getAttribute("passwordUguali"); 
	}
	%>
	<a href = "main2"> Torna alla Pagine Principale </a>
	<%
	if(admin.equals("true")){
	%>	
		<p>Pagina admin </p>
		<%
		if(erroreDatiInseriti.equals("true")){
		%>
			<p style = "color:red">I dati inseriti non sono corretti </p>
		<%
		}
		%>
		<p>Inserire i dati admin per accedere: </p>
		<form method = "post" action = "AdminLoginServlet">
			<label for = "user">Username: </label>
			<input id = "user" type = "text" name = "aName">
			<br>
			<label for = "pass" >Password: </label>
			<input id = "pass" type = "password" name = "aPsw">
			<br>
			<input type = "submit" value = "Login">
		</form>
	<%
	} else {
	%>
	<p>Pagina utente </p>
		<%
		if(erroreDatiInseriti.equals("true")){
		%>
			<p style = "color:red">I dati inseriti non sono corretti </p>
		<%
		}
		%>
	<p>Inserire i dati utente per accedere: </p>	
	<form method = "post" action = "UserLoginServlet">
		<label for = "user">Username: </label>
		<input id = "user" type = "text" name = "aName">
		<br>
		<label for = "pass" >Password: </label>
		<input id = "pass" type = "password" name = "aPsw">
		<br>
		<input type = "submit" value = "Login">
	</form>	
	<form method = "post" action= "RegistrazioneServlet">
		<p>Inserire i dati utente per registrarsi: </p>	
		<%
		if(erroreDatiRegistrazione.equals("true")){
		%>
			<p style = "color:red">Dati mancanti per la registrazione</p>
		<%
		}
		%>
		<%
		if(!passwordUguali.equals("true")){
		%>
			<p style = "color:red">Le password non coincidono</p>
		<%
		}
		%>
		<label for = "user">Username: </label>
		<input id = "user" type = "text" name = "user">
		<br>
		<label for = "pass" >Password: </label>
		<input id = "pass" type = "password" name = "pass">
		<br>
		<label for = "pass2" >Reinserisci Password: </label>
		<input id = "pass2" type = "password" name = "pass2">
		<br>
		<label for = "nome" >Inserisici il tuo Nome: </label>
		<input id = "nome" type = "text" name = "nome">
		<br>
		<label for = "cognome" >Inserisci il tuo Cognome: </label>
		<input id = "cognome" type = "text" name = "cognome">
		<br>
		<label for = "via" >Inserisci la tua Via: </label>
		<input id = "via" type = "text" name = "via">
		<label for = "numeroVia" >Numero: </label>
		<input id = "numeroVia" type = "number" name = "numeroVia">
		<label for = "cap" >Cap: </label>
		<input id = "cap" type = "number" name = "cap">
		<label for = "paese" >Paese: </label>
		<input id = "paese" type = "text" name = "paese">
		<br>
		<input type = "submit" value = "Registrati">
	</form>
	<%
	}
	%> 
</body>
</html>