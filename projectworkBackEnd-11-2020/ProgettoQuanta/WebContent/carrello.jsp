<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import = "com.quanta.model.Prod" %>
<%@ page import = "com.quanta.model.Imm" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
	int prezzoTotale = 0;
	if(request.getAttribute("listaProdottiNelCarrello") != null){
		List<Prod> listaProdottiNelCarrello =  (List<Prod>)request.getAttribute("listaProdottiNelCarrello");
	%>
		<a href = "main2">Torna alla Pagina Principale</a>
		<table style = "border:2px; border-color:red; border-style:solid;border-width: 2px">
				<caption>Prodotti</caption>
				<thead> 
					<tr> 
						<th> Nome </th>
						<th> Immagine</th>
						<th> Descrizione</th>
						<th> Quantita </th>
						<th> Prezzo </th>
					</tr>
				</thead>
				<tbody>
					<% 
					for(int i = 0; i < listaProdottiNelCarrello.size(); i++){
					%>
						<tr> 
							<td> 
							<%= listaProdottiNelCarrello.get(i).getpName() %>
							</td>
							<td> 
							<% 
							List<Imm> listImm = listaProdottiNelCarrello.get(i).getListaImm(); 
							if(listImm.size() == 0){
							%>
								Empty1
							<% 
							} else{
								if(listImm.get(0) == null){
							%>
										Empty2
								<%
								} else {
									Imm pri = null;
									for(Imm imm : listImm){
										if(imm.isPri()){
											pri = imm;
											%>
											<img src = "<%= pri.getUrl()%>" width="256" height="192">
											<%
										}
									}
									if(pri == null){
										%>
										<img src = "<%= listImm.get(0).getUrl()%>" width="256" height="192">
										<%
									}
								}
							}
							%>
							</td>
							<td>
								<%= listaProdottiNelCarrello.get(i).getpDesc() %>
							</td>
							<td>
								<form action="ModificaQuantitaCarrelloServlet">
									<input type = "hidden" name = "posizioneProdottoNellaLista" value = "<%= i%>">
									<input type = "number" id = "quantita" name = "quantita" value = "<%= listaProdottiNelCarrello.get(i).getpQuant() %>" min = "0">
									<input type = "submit" value = "Modifica">
								</form>
									
							</td>
							<td>
								<%prezzoTotale = prezzoTotale +  listaProdottiNelCarrello.get(i).getpPrezzo() * listaProdottiNelCarrello.get(i).getpQuant();%>
								<%= listaProdottiNelCarrello.get(i).getpPrezzo() * listaProdottiNelCarrello.get(i).getpQuant() %>
							</td>
						</tr>
					<%
					} 
					%>
						<tr>
							<td style="font-weight: bold;font-size: x-large;">Prezzo totale: </td>
							<td style="font-weight: bold;font-size: x-large;"><%=prezzoTotale %> </td>
							<td> </td>
							<td> </td>
							<td> </td>
						</tr>
				</tbody>
			</table>
			<form method = "post" action="VerifyLoginServlet">
				<input type = "submit" value = "Prodcedi con il Pagamento">
			</form>
	<%
	} else {
	%>
		<p style ="color: red; size: 10px;">Il carrello � vuoto </p>
	<%
	}
	%>
</body>
</html>