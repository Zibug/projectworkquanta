<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>  
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "com.quanta.model.Cat" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Categorie</title>
</head>
<body>
	<% String aName = (String)request.getAttribute("aName"); %>
	<a href = "main2"> Torna alla Pagine Principale </a>
	<br>
	<a href = "gestioneAdmin.jsp?aName=<%=aName%>">Torna alla pagina dell'admin</a>
	<% List<Cat> listCat = (ArrayList<Cat>)request.getAttribute("lista"); %>
	
	<table style = "border:2px; border-color:red; border-style:solid;border-width: 2px">
		<caption>Categorie</caption>
		<thead> 
			<tr> 
				<th> Nome </th>
			</tr>
		</thead>
		<tbody>
		<% 
		for(int i = 0; i < listCat.size(); i++){
		%>
			<tr> 
				<td> 
				<%= listCat.get(i).getcName() %>
				</td>
				<td> 
					<form method = "post" action="FindAllProdServlet">
						<input type = "hidden" name = "cName" value = "<%= listCat.get(i).getcName() %>">
						<input type = "hidden" name = "id" value = "<%= listCat.get(i).getcId() %>">
						<input type = "submit" value = "Visualizza">
					</form>
				</td>
				<td>
					<form method = "post" action="EditCatServlet">
						<input type = "hidden" name = "cName" value = "<%= listCat.get(i).getcName() %>">
						<input type = "hidden" name = "id" value = "<%= listCat.get(i).getcId() %>">
						<input type = "submit" value = "Modifica">
					</form>
				</td>
				<td>
					<form method = "post" action="DeleteCatServlet">
						<input type = "hidden" name = "cId" value = "<%= listCat.get(i).getcId() %>">
						<input type = "submit" value = "Delete">
					</form>
				</td>
			</tr>
		<% } %>
		</tbody>
	</table>
	<form method = "post" action="AddCatServlet">
		<input type = "submit" value = "Aggiungi Categoria">
	</form>
</body>
</html>




































