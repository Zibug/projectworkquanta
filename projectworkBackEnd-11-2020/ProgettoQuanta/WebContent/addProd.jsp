<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%-- Variabili in entrata --%>
	<% boolean errore = (boolean)request.getAttribute("errore");%>
	<% int cId = (Integer)request.getAttribute("cId"); %>
	<%-- ------------------------------------------------------ --%>
	<form method = "post" action = "AddProdServlet2">
		<input type = "hidden" name = "cId" value = "<%= cId%>">
		<p>Inserire le caratteristiche del nuovo prodotto da inserire nel database:</p>
		<%
			if(errore){
		%>
				<p style="font-size: x-large; color: red">- Errore completare tutti i campi </p>
		<%		
			}
		%>
		<label for = "nome">Nome: </label>
		<input type = "text" id = "nome" name = "pName" placeholder = "es. computer">
		<br>
		<label for = "descrizione">Descrizione: </label>
		<input type = "text" id = "descrizione" name = "pDesc" placeholder = "es. molto brutto">
		<br>
		<label for = "quantita">Quantita: </label>
		<input type = "number" id = "quantita" name = "pQuant" placeholder = "es. 35">
		<br>
		<label for = "prezzo">Prezzo: </label>
		<input type = "number" id = "prezzo" name = "pPrezzo" placeholder = "es. 1200">
		<br>
		<label for = "url" >Inserisici URL nuova immagine da inserire: </label>
		<input type = "text" id = "url" name = "url" placeholder = "es. http://www.immaginipotenti.com/prova.jpg">
		<input type = "submit" value = "Conferma">
	</form>
	<a href = "FindAllCatServlet">Torna a categorie di prodotti</a>
</body>
</html>