<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% boolean errore = (boolean)request.getAttribute("errore"); %>
	<% int cId = (Integer)request.getAttribute("cId"); %>
	<% String cNome = (String)request.getAttribute("cNome"); %>
	<form method = "post" action = "EditCatServlet2">
		<p>Inserire le nuove caratteristiche:</p>
		<%
			if(errore){
		%>	
				<p style="font-size: x-large; color: red">- Errore completare tutti i campi </p>
		<%
			}
		
		%>
		<input type = "hidden" name = "cId" value = "<%= cId%>">
		<label for = "nome">Nome: </label>
		<input type = "text" id = "nome" name = "cNome" value = "<%= cNome%>">
		<input type = "submit" value = "Conferma">
	</form>
</body>
</html>