<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% String erroreDatiInseriti = (String)request.getAttribute("erroreDatiInseriti"); %>
	<% String erroreDatiRegistrazione = (String)request.getAttribute("erroreDatiRegistrazione");  %>
	<% String passwordUguali = (String)request.getAttribute("passwordUguali"); %>
	Per procedere con il pagamento � necessario effetuare il login o registrarsi
	<%
	if(erroreDatiInseriti.equals("true")){
	%>
		<p style = "color:red">I dati inseriti non sono corretti </p>
	<%
	}
	%>
	<p>Inserire i dati utente per accedere: </p>	
	<form method = "post" action = "UserLoginPagamentoServlet">
		<label for = "user">Username: </label>
		<input id = "user" type = "text" name = "aName">
		<br>
		<label for = "pass" >Password: </label>
		<input id = "pass" type = "password" name = "aPsw">
		<br>
		<input type = "submit" value = "Login">
	</form>	
	<form method = "post" action= "RegistrazionePagamentoServlet">
		<p>Inserire i dati utente per registrarsi: </p>	
		<%
		if(erroreDatiRegistrazione.equals("true")){
		%>
			<p style = "color:red">Dati mancanti per la registrazione</p>
		<%
		}
		%>
		<%
		if(!passwordUguali.equals("true")){
		%>
			<p style = "color:red">Le password non coincidono</p>
		<%
		}
		%>
		<label for = "user">Username: </label>
		<input id = "user" type = "text" name = "user">
		<br>
		<label for = "pass" >Password: </label>
		<input id = "pass" type = "password" name = "pass">
		<br>
		<label for = "pass2" >Reinserisci Password: </label>
		<input id = "pass2" type = "password" name = "pass2">
		<br>
		<label for = "nome" >Inserisici il tuo Nome: </label>
		<input id = "nome" type = "text" name = "nome">
		<br>
		<label for = "cognome" >Inserisci il tuo Cognome: </label>
		<input id = "cognome" type = "text" name = "cognome">
		<br>
		<label for = "via" >Inserisci la tua Via: </label>
		<input id = "via" type = "text" name = "via">
		<label for = "numeroVia" >Numero: </label>
		<input id = "numeroVia" type = "number" name = "numeroVia">
		<label for = "cap" >Cap: </label>
		<input id = "cap" type = "number" name = "cap">
		<label for = "paese" >Paese: </label>
		<input id = "paese" type = "text" name = "paese">
		<br>
		<input type = "submit" value = "Registrati">
	</form>
</body>
</html>