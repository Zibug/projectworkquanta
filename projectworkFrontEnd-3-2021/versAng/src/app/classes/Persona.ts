export class Persona {
    id: number = 0;
    nome: string = "";
    congome: string = "";
    codiceFiscale: string = "";
    genere: string ="";
    dataN: Date = new Date();
    eta: number = 0;
    contratto: string="";
    email: string="";
    psw: string="";
    constructor(id?: number, nome?: string, cognome?: string, codiceFiscale?: string, genere?: string, dataN?: Date, eta?: number, contratto?: string, email?: string, psw?: string) {
        this.id = id;
        this.nome = nome;
        this.congome = cognome;
        this.codiceFiscale = codiceFiscale;
        this.genere = genere;
        this.dataN = dataN;
        this.eta = eta;
        this.contratto = contratto;
        this.email = email;
        this.psw = psw;
    }
}