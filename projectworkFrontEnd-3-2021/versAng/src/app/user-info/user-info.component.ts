import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Persona } from '../classes/Persona';
import { Persone } from '../services/services';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  @Input() user:Persona;
  iconClose = faWindowClose;
  @Output() close = new EventEmitter();
  constructor(private service:Persone) { 
    this.user = service.persone[1];  
  }

  ngOnInit(): void {
  }
  _onClose(){
    this.close.emit(undefined);
  }
}
