import { Component, OnInit } from '@angular/core';
import { Persona } from '../classes/Persona';
import { Persone } from '../services/services';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: Persona[];
  show:Persona = undefined;
  constructor(private lista: Persone) { 
    
  }

  ngOnInit(): void {
    this.users = this.lista.persone;
  }
  _onShowInfo(show:Persona){
    if(this.show == undefined){
      this.show = show;
    } else {
      this.show = undefined;
      this.show = show;
    }
  }
  _onClose(value:any){
    this.show = value;
  }
}
