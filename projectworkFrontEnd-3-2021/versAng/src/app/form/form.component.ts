import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { Persona } from '../classes/Persona';
import { Persone } from '../services/services';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  myForm: FormGroup;
  user: Persona;
  pswConf: string;
  savedDiv: boolean = false;
  iconClose = faWindowClose;
  get myFCont() {
    return this.myForm.controls;
  }
  constructor(private service: Persone, private fb: FormBuilder) {

  }
  ngOnInit(): void {
    this.user = new Persona();
    this.user.id = this.service.persone.length+ 1;
    this.myForm = this.fb.group({
      // https://angular.io/guide/form-validation
      nome: ["", [
        Validators.required
      ]],
      cognome: ["",
        [
          Validators.required
        ]
      ],
      codiceFiscale: ["",
        [
          Validators.required,
          Validators.pattern(/[A-Z]{6}\d{2}[A-Z]\d{2}\w{4}[A-Z]/),
          this.controlloCF(this.service.persone)
        ]
      ],
      genere: ["M",
        [
          Validators.required
        ]
      ],
      dataN: ["",
        [
          Validators.required
        ]
      ],
      eta: [0,
        [
          Validators.required
        ]
      ],
      contratto: ["stage",
        [
          Validators.required
        ]
      ],
      email: ["",
        [
          Validators.required,
          Validators.pattern(/\w*@\w*\..{1,3}/i),
          this.controlloMail(this.service.persone)
        ]
      ],
      password: ["",
        [
          Validators.required,
          Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/)
          // this.controlloPsw()
        ]
      ],
      passwordConf: ["",
        [
          Validators.required
          // this.controlloPsw()
        ]
      ]
    })
    this.myForm.valueChanges.subscribe((v) => {
      this.user.nome = v.nome;
      this.user.congome = v.cognome;
      this.user.codiceFiscale = v.codiceFiscale;
      this.user.genere = v.genere;
      this.user.dataN = v.dataN;
      this.ageCalc();
      v.eta = this.user.eta;
      this.user.eta = v.eta;
      this.user.contratto = v.contratto;
      this.user.email = v.email;
      this.user.psw = v.password;
      this.pswConf = v.passwordConf;

    });
  }

  formRestart(){
    this.myForm = this.fb.group({
      // https://angular.io/guide/form-validation
      nome: ["", [
        Validators.required
      ]],
      cognome: ["",
        [
          Validators.required
        ]
      ],
      codiceFiscale: ["",
        [
          Validators.required,
          Validators.pattern(/[A-Z]{6}\d{2}[A-Z]\d{2}\w{4}[A-Z]/),
          this.controlloCF(this.service.persone)
        ]
      ],
      genere: ["M",
        [
          Validators.required
        ]
      ],
      dataN: ["",
        [
          Validators.required
        ]
      ],
      eta: [0,
        [
          Validators.required
        ]
      ],
      contratto: ["stage",
        [
          Validators.required
        ]
      ],
      email: ["",
        [
          Validators.required,
          Validators.pattern(/\w*@\w*\..{1,3}/i),
          this.controlloMail(this.service.persone)
        ]
      ],
      password: ["",
        [
          Validators.required,
          Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/)
          // this.controlloPsw()
        ]
      ],
      passwordConf: ["",
        [
          Validators.required
          // this.controlloPsw()
        ]
      ]
    })
  }

  ageCalc() {
    var eta = this.user.eta;
    var oggi = new Date();
    var oggiG, oggiM, oggiA;
    oggiG = oggi.getDate();
    oggiM = oggi.getMonth() + 1; //i mesi vanno da 0 a 11
    oggiA = oggi.getFullYear();
    var dataN = new Date(this.user.dataN);
    var dataNG = dataN.getDate();
    var dataNM = dataN.getMonth() + 1;
    var dataNA = dataN.getFullYear();
    if (oggiM > dataNM) { //Se il mese corrente è maggiore del mese della nascita
      eta = oggiA - dataNA; //ho fatto il compleanno
    } else if (oggiM == dataNM) { //Il mese corrente è il mese del mio compleanno
      if (oggiG >= dataNG) {
        eta = oggiA - dataNA; //ho fatto il compleanno
      } else {
        eta = oggiA - dataNA - 1; // non ho fatto ancora il compleanno
      }
    } else if (oggiM < dataNM) { //Il mese del mio compleanno deve ancora arrivare
      eta = oggiA - dataNA - 1;
    }
    this.user.eta = eta;
  }

  controlloMail(lista: Persona[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let exist: boolean = false;
      for (let index = 0; index < lista.length; index++) {
        if (lista[index].email == control.value) {
          exist = true; 
          return { 'emailInvalid': exist };
        }
      }
      return null;

      // lista.forEach(element => {
      //   if (element.email == control.value) {
      //     return { 'emailInvalid': true }
      //   }
      // });
    };
  }

  controlloCF(lista: Persona[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let exist: boolean = false;
      for (let index = 0; index < lista.length; index++) {
        if (lista[index].codiceFiscale == control.value) {
          exist = true;
          return { 'cFInvalid': exist };
        }
      }
      return null;
    }
  }

  saveData(){
    this.service.saveData(this.user);
    this.formRestart();
  }
}

// function ValidateEmail(control: AbstractControl): {[key: string]: any} | null  {
//   if (control.value && control.value.length != 10) {
//     return { 'phoneNumberInvalid': true };
//   }
//   return null;
// }


// boolean esiste = true;
// string targaInserita;
// while(esiste){
//   targaInserita = console
//   for(i; i<listaAuto.size; i++){
//     if(targaInserita != listaAuto[i].targa){
//       esiste = false;
//     }
//   }
// }
