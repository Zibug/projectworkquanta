import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Persona } from '../classes/Persona';
import { faUserMinus, faUser } from '@fortawesome/free-solid-svg-icons'
import { Persone } from '../services/services';

@Component({
  selector: 'tr[app-user]',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private user:Persona;
  iconaDel = faUserMinus;
  icona = faUser;
  id:number = -1;
  @Input() set _user(user:Persona){
    this.user = user;
  }
  get _user(){
    return this.user;
  }
  @Output() showInfo = new EventEmitter<Persona>();
  constructor(private service:Persone) { 
  }

  ngOnInit(): void {
    this.id = this._user.id;
  }
  _onShowInfo(){
    this.showInfo.emit(this._user);
  }
  _onDeleteUser(){
    this.service.deleteData(this.user);
  }
}
