import { Injectable } from "@angular/core";
import { Persona } from "../classes/Persona";

@Injectable()
export class Persone {
    persone: Array<Persona> = [];
    constructor() {
        var data1 = new Date("01/01/2001");
        var data2 = new Date("02/02/2002");
        var data3 = new Date("03/03/2003");
        var data4 = new Date("04/04/2004");
        this.persone.push(new Persona(1, "nome1", "cognome1", "AAAAAA11A11A11AA", "M", data1, 11, "contratto1", "mail1@mail.it", "password1"));
        this.persone.push(new Persona(2, "nome2", "cognome2", "BBBBBB22B22B22BB", "M", data2, 22, "contratto2", "mail2@mail.it", "password2"));
        this.persone.push(new Persona(3, "nome3", "cognome3", "CCCCCC33C33C33CC", "M", data3, 33, "contratto3", "mail3@mail.it", "password3"));
        this.persone.push(new Persona(4, "nome4", "cognome4", "DDDDDD44D44D44DD", "M", data4, 44, "contratto4", "mail4@mail.it", "password4"));
    }

    saveData(newPersona: Persona) {
        console.log(newPersona.nome);
        this.persone.splice(0,0,newPersona);
        console.log(this.persone);
    }
    deleteData(persona:Persona){
        let codiceF:string = persona.codiceFiscale;
        let pos = -1;
        for (let index = 0; index < this.persone.length; index++) {
            if(codiceF == this.persone[index].codiceFiscale){
                pos = index;
            }
        }
        this.persone.splice(pos,1);
    }
}