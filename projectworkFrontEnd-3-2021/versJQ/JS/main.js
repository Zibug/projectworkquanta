// class Persona {
//     constructor(nome, cognome, codiceFiscale){
//         this.nome = nome;
//         this.cognome = cognome;
//         this.codiceFiscale = codiceFiscale;
//         this.genere = "";
//         this.dataN = "";
//         this.eta = "";
//         this.contratto = "";
//         this.email = "";
//         this.psw = "";
//     }
// }

// var personeLS = [
//     {
//         "nome" : "nome1",
//         "cognome" : "congnome1",
//         "codiceFiscale" : "AAAAAA11A11A11AA"
//     },
//     {
//         "nome" : "nome2",
//         "cognome" : "congnome2",
//         "codiceFiscale" : "BBBBBB22B22B22BB"
//     },
//     {
//         "nome" : "nome3",
//         "cognome" : "congnome3",
//         "codiceFiscale" : "CCCCCC33C33C33CC"
//     },
//     {
//         "nome" : "nome4E",
//         "cognome" : "congnome4",
//         "codiceFiscale" : "DDDDDD44D44D44DD"
//     }
// ]

class Persona {
    constructor(nome, cognome, codiceFiscale, genere, dataN, eta, contratto, email, psw){
        this.nome = nome;
        this.cognome = cognome;
        this.codiceFiscale = codiceFiscale;
        this.genere = genere;
        this.dataN = dataN;
        this.eta = eta;
        this.contratto = contratto;
        this.email = email;
        this.psw = psw;
    }
    toString() {
        return (
            "nome: " + nome + 
            "cognome: " + cognome + 
            "codiceFiscale: " + codiceFiscale + 
            "genere: " + genere + 
            "dataN: " + dataN + 
            "eta: " + eta + 
            "contratto: " + contratto + 
            "email: " + email + 
            "psw: " + psw
        )
    }
}

var personaProva1 = new Persona("nome1", "cognome1", "AAAAAA11A11A11AA", "M", "", "", "", "mail1@mail.it", "");
var personaProva2 = new Persona("nome2", "cognome2", "BBBBBB22B22B22BB", "M", "", "", "", "mail2@mail.it", "");
var personaProva3 = new Persona("nome3", "cognome3", "CCCCCC33C33C33CC", "M", "", "", "", "mail3@mail.it", "");
var personaProva4 = new Persona("nome4", "cognome4", "DDDDDD44D44D44DD", "M", "", "", "", "mail4    @mail.it", "");
var personaProvaChristian = new Persona("christian", "sibug", "SBGCRS94H22F205Y", "M", "22/06/1994", "26", "stage", "sibugchristian@gmail.com", "Password1");

var personeLS = [personaProva1, personaProva2, personaProva3, personaProva4, personaProvaChristian];

window.localStorage.setItem("persone",  JSON.stringify(personeLS));

/*****************************TABLEBODY***************************************/
//TABLELOAD
function tableLoad(){
    let persone = JSON.parse(window.localStorage.getItem("persone"));
    for (let index = 0; index < persone.length; index++) {
        let persona = persone[index];
        if(persona != null){
            document.getElementById("tableBody").innerHTML += `<tr><td class="tdd"><button id="tbd${index}" class="tbt" onclick="delPersona(${index})"><img src="img/remove.png" alt=""></img></button></td><td>${persona.nome}</td><td>${persona.cognome}</td><td>${persona.codiceFiscale}</td><td class="tdv"><button id="tbt${index}" class="tbt" onclick="infoPersona(${index})"><img src="img/person_icon-icons.com_50075.png" alt=""></img></button></td></tr>`;
            
        }
    }
}
window.addEventListener("load", tableLoad);