 //FORMVALIDATIONS
var formA = document.getElementById("formAggio");
formA.codiceFiscale.addEventListener("input", function(e){ //Evento input, si puo fare anche con change ma fa la verifica ad ogni cambiamento
    if(!/[A-Z]{6}\d{2}[A-Z]\d{2}\w{4}[A-Z]/.test(formA.codiceFiscale.value)){ //Settaggio validazione con RegEx
        formA.codiceFiscale.setCustomValidity("Errore, formato non rispettato");
    } else {
        formA.codiceFiscale.setCustomValidity("");
    }
});

formA.codiceFiscale.addEventListener("input", function(e){ //Evento input, si puo fare anche con change ma fa la verifica ad ogni cambiamento
    var personaLS = JSON.parse(window.localStorage.getItem("persone"));
    var esiste = false;
    for (let index = 0; index < personaLS.length; index++) {
        let persona = personaLS[index];
        if(persona.codiceFiscale == formA.codiceFiscale.value){
            esiste = true;
        }
    }
    if(esiste){ 
        formA.codiceFiscale.setCustomValidity("Codice Fiscale gia presente nel database");
    } else {
        formA.codiceFiscale.setCustomValidity("");
    }    
});

formA.pass.addEventListener("input", function(e){//VALIDAZIONE PASSWORD
    if(!/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/.test(formA.pass.value)){
        formA.pass.setCustomValidity("La password deve contenere almeno 1 Maiuscola, 1 Minuscola e 8 caratteri");
    } else {
        formA.pass.setCustomValidity("");
    }
});

formA.mail.addEventListener("input", function(e){
    if(!/\w*@\w*\..{2,3}/i.test(formA.mail.value)){
        formA.mail.setCustomValidity("Errore, formato non rispettato");
    } else {        
        formA.mail.setCustomValidity("");
    }
});

formA.mail.addEventListener("input", function(e){
    var personaLS = JSON.parse(window.localStorage.getItem("persone"));
    var esiste = false;
    for (let index = 0; index < personaLS.length; index++) {
        let persona = personaLS[index];
        if(persona.email == formA.mail.value){
            esiste = true;
        }
    }
    if(esiste){
        formA.mail.setCustomValidity("Email gia presente nel database");
    } else {
        formA.mail.setCustomValidity("");
    }   
});

function verificaPassword(){
    var pass1 = formA.pass.value;
    var pass2 = formA.passConf.value;
    if(pass1 != pass2){
        formA.passConf.setCustomValidity("Errore, le password non coincidono");
    } else {
        formA.passConf.setCustomValidity("");
    }
}

formA.pass.addEventListener("input", verificaPassword);
formA.passConf.addEventListener("input", verificaPassword);
formA.addEventListener("submit", verificaPassword);

//AGECALCULATOR
formA.dataN.addEventListener("input", function(e){
    var dataN = document.getElementById("dataN").value; 
    var eta = calcolaEta(dataN);
    document.getElementById("eta").value = eta;
});

function calcolaEta(data){
    var oggi = new Date();
    var oggiG, oggiM, oggiA;
    oggiG = oggi.getDate();
    oggiM = oggi.getMonth() + 1; //i mesi vanno da 0 a 11
    oggiA = oggi.getFullYear();

    var dataN = new Date(data);
    dataNG = dataN.getDate();
    dataNM = dataN.getMonth() + 1;
    dataNA = dataN.getFullYear();

    if(oggiM > dataNM){ //Se il mese corrente è maggiore del mese della nascita
        eta = oggiA - dataNA; //ho fatto il compleanno
    } else if (oggiM = dataNM){ //Il mese corrente è il mese del mio compleanno
        if(oggiG >= dataNG){
            eta = oggiA - dataNA; //ho fatto il compleanno
        } else {
            eta = oggiA - dataNA - 1; // non ho fatto ancora il compleanno
        }
    } else if(oggiM < dataNM){ //Il mese del mio compleanno deve ancora arrivare
        eta = oggiA - dataNA - 1;
    }

    console.log(eta);

    return eta;

    console.log(`${dataNG}/${dataNM}/${dataNA}`);
    

    // var dataN = new Date(); 
    // dataN = Date.parse(document.getElementById("dataN"));
    // dataNG = dataN.getDate();
    // dataNM = dataN.getMonth() + 1;
    // dataNA = dataN.getFullYear();
    // console.log(`${dataNG}/${dataNM}/${dataNA}`);
}

//DELETEDATA
function delPersona(index){
    var personeJS = JSON.parse(window.localStorage.getItem("persone"));
    window.localStorage.removeItem("persone");
    personeJS.splice(index, 1);
    window.localStorage.setItem("persone", JSON.stringify(personeJS));
    document.getElementById("tableBody").innerHTML = "";
    tableLoad();
}

function addPersona(){  
    var nome = $("#nome").val();
    var cognome = $("#cognome").val();
    var codiceFiscale = $("#codiceFiscale").val();
    var genere = $('input[name="sess"]:checked').val();
    console.log(formA.sess.value);
    var dataN = $("#dataN").val();
    var eta = $("#eta").val();
    var contratto = $("#selMansione").val();
    var email = $("#mail").val();
    var psw = $("#pass").val();
    var personaNew = new Persona(nome, cognome, codiceFiscale, genere, dataN, eta, contratto, email, psw);
    var personeLS = JSON.parse(window.localStorage.getItem("persone"));
    window.localStorage.removeItem("persone");
    personeLS.push(personaNew);
    window.localStorage.setItem("persone", JSON.stringify(personeLS));
    document.getElementById("tableBody").innerHTML = "";
    // alert(nome + "\n" + cognome + "\n" + codiceFiscale + "\n" + genere + "\n" + dataN + "\n" + eta + "\n" + contratto + "\n" + email + "\n" + psw);
    tableLoad();
}

function infoPersona(index){
    document.getElementById("bloccoPersona").style.display = "block";
    var personeLS = JSON.parse(window.localStorage.getItem("persone"));
    document.getElementById("nomeD").innerHTML = personeLS[index].nome;
    document.getElementById("cognomeD").innerHTML = personeLS[index].cognome;
    document.getElementById("codiceFiscaleD").innerHTML = personeLS[index].codiceFiscale;
    document.getElementById("sessD").innerHTML = personeLS[index].genere;
    document.getElementById("dataND").innerHTML = personeLS[index].dataN;
    document.getElementById("etaD").innerHTML = personeLS[index].eta;
    document.getElementById("mansioneD").innerHTML = personeLS[index].contratto;
    document.getElementById("mailD").innerHTML = personeLS[index].email;
}

// $("#confermaForm").click(function(){
//     var nome = $("#nome").val();
//     var cognome = $("#cognome").val();
//     var codiceFiscale = $("#codiceFiscale").val();
//     var genere = $('input[name="sess"]:checked').val();
//     var dataN = $("#dataN").val();
//     var eta = $("#eta").val();
//     var contratto = $("#selMansione").val();
//     var email = $("#mail").val();
//     var psw = $("#pass").val();
//     var personaNew = new Persona(nome, cognome, codiceFiscale, genere, dataN, eta, contratto, email, psw);
//     var personeLS = JSON.parse(window.localStorage.getItem("persone"));
//     window.localStorage.removeItem("persone");
//     personeLS.push(personaNew);
//     window.localStorage.setItem("persone", JSON.stringify(personeLS));
//     // alert(nome + "\n" + cognome + "\n" + codiceFiscale + "\n" + genere + "\n" + dataN + "\n" + eta + "\n" + contratto + "\n" + email + "\n" + psw);
//     tableLoad();
// });
